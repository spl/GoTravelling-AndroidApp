#项目说明
##简介
提供旅游路线自定制与分享服务的Android App项目

##Android版本
* 目标版本为Android 4.2
* 最低版本为Android 2.3

##手机分辨率
开启界面的时候Android运行时动态去获取界面的分辨率，然后根据实际屏幕属性自动加载不同的配置

##IDE和构建工具
开发工具使用[Eclipse+ADT](http://jingyan.baidu.com/article/b0b63dbfa9e0a74a4830701e.html)，并使用[Gradle工具](http://my.oschina.net/moziqi/blog/308842)构建项目

##相关技术点
* 基本地图展现、搜索、定位、逆/地理编码、路线规划等功能基于[百度地图API](http://developer.baidu.com/map/)实现
* Android 网络通信使用[Volley](http://blog.csdn.net/t12x3456/article/details/9221611)框架

##其他说明
待完善