package com.teamwork.daily;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteLine;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteLine;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.teamwork.gotravelling.MainActivity;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Sight;
import com.teamwork.model.Transport;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
import com.teamwork.utils.MyOnKeyListener;

public class AddTransportActivity extends Activity
{
	public static final String ROUTE_INDEX = "route_index";
	public static final String ADD_TRANSPORT = "add_transport";
	
	public static final int FROM_SIGHT_REQUEST = 123;
	public static final int TO_SIGHT_REQUEST = 124;
	
	private int mRouteIndex;
	
	//记录当前的焦点在那个button上，默认为0
	private int mIndex = 0;
	private Button mCarButton;
	private Button mBusButton;
	private Button mWalkButton;
	
	private EditText mFromEditText;
	private EditText mToEditText;
	
	private RadioButton mFromLocation;
	private RadioButton mFromFavorite;
	private RadioButton mToLocation;
	private RadioButton mToFavorite;
	
	private Sight mFromSight = new Sight();
	private Sight mToSight = new Sight();
	
	private RequestQueue mQueue;
	private ProgressDialog mAddProgressDialog;
	
	//路径规划搜索
	private RoutePlanSearch mSearch;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_transport);
		Intent intent = getIntent();
		mRouteIndex = intent.getIntExtra(ROUTE_INDEX, 0);
		
		mQueue = Volley.newRequestQueue(getApplicationContext());
		mAddProgressDialog = HandleUtil.showProgressDialog(this, "正在增加...");
		
		Button backButton = (Button)findViewById(R.id.btn_back);
		backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
		
		mCarButton = (Button)findViewById(R.id.btn_car);
		mCarButton.setBackgroundResource(R.drawable.car);
		mCarButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				if (mIndex != 0)
				{
					changeButton(0);
					mIndex = 0;
				}
			}
		});
		
		mBusButton = (Button)findViewById(R.id.btn_bus);
		mBusButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				if (mIndex != 1)
				{
					changeButton(1);
					mIndex = 1;
				}
			}
		});
		
		mWalkButton = (Button)findViewById(R.id.btn_walk);
		mWalkButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				if (mIndex != 2)
				{
					changeButton(2);
					mIndex = 2;
				}
			}
		});
		
		mFromEditText = (EditText)findViewById(R.id.et_from);
		mFromEditText.setOnKeyListener(new MyOnKeyListener());
		mFromEditText.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) 
			{
				mFromSight.setName(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) 
			{
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0)
			{
				
			}
		});
		
		mToEditText = (EditText)findViewById(R.id.et_to);
		mToEditText.setOnKeyListener(new MyOnKeyListener());
		mToEditText.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) 
			{
				mToSight.setName(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) 
			{
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0)
			{
				
			}
		});
		
		mFromLocation = (RadioButton)findViewById(R.id.rb_from_location);
		mFromFavorite = (RadioButton)findViewById(R.id.rb_from_favorite);
		mToLocation = (RadioButton)findViewById(R.id.rb_to_location);
		mToFavorite = (RadioButton)findViewById(R.id.rb_to_favorite);
		
		RadioGroup fromRadioGroup = (RadioGroup)findViewById(R.id.rg_from_select);
		fromRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int checkedId) 
			{
				if (checkedId == R.id.rb_from_location)
				{
					getStartLocation();
					mFromLocation.setChecked(false);
				}
				else if (checkedId == R.id.rb_from_favorite)
				{
					Intent intent = new Intent(AddTransportActivity.this, GetDailySightActivity.class);
					startActivityForResult(intent, FROM_SIGHT_REQUEST);
					mFromFavorite.setChecked(false);
				}
			}
		});
		
		RadioGroup toRadioGroup = (RadioGroup)findViewById(R.id.rg_to_select);
		toRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup radioGroup, int checkedId) 
			{
				if (checkedId == R.id.rb_to_location)
				{
					getEndLocation();
					mToLocation.setChecked(false);
				}
				else if (checkedId == R.id.rb_to_favorite)
				{
					Intent intent = new Intent(AddTransportActivity.this, GetDailySightActivity.class);
					startActivityForResult(intent, TO_SIGHT_REQUEST);
					mToFavorite.setChecked(false);
				}
			}
		});
		
		Button previewButton = (Button)findViewById(R.id.btn_preview);
		previewButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				preview();
			}
		});
		
		Button addButton = (Button)findViewById(R.id.btn_add);
		addButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				addTransport();
			}
		});
		
		mSearch = RoutePlanSearch.newInstance();
		mSearch.setOnGetRoutePlanResultListener(new OnGetRoutePlanResultListener() 
		{
			@Override
			public void onGetWalkingRouteResult(WalkingRouteResult result)
			{
				if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) 
				{
					mAddProgressDialog.dismiss();
					
		            Toast.makeText(AddTransportActivity.this, "增加失败，请确保起点和终点信息无误", Toast.LENGTH_SHORT).show();
		        }
				if (result.error == SearchResult.ERRORNO.NO_ERROR) 
				{
					WalkingRouteLine walkLine = result.getRouteLines().get(0);
					RouteNode startNode = walkLine.getStarting();
					RouteNode endNode = walkLine.getTerminal();
					int time = walkLine.getDuration();
					int distance = walkLine.getDistance();
					putTransportData(startNode, endNode, "步行", time, distance);
		        }
			}
			
			@Override
			public void onGetTransitRouteResult(TransitRouteResult result) 
			{
				if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) 
				{
					mAddProgressDialog.dismiss();
					
		            Toast.makeText(AddTransportActivity.this, "增加失败，请确保起点和终点信息无误", Toast.LENGTH_SHORT).show();
		        }
				if (result.error == SearchResult.ERRORNO.NO_ERROR) 
				{
					TransitRouteLine transitLine = result.getRouteLines().get(0);
					RouteNode startNode = transitLine.getStarting();
					RouteNode endNode = transitLine.getTerminal();
					int time = transitLine.getDuration();
					int distance = transitLine.getDistance();
					putTransportData(startNode, endNode, "公交", time, distance);
		        }
			}
			
			@Override
			public void onGetDrivingRouteResult(DrivingRouteResult result)
			{
				if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) 
				{
					mAddProgressDialog.dismiss();
					
		            Toast.makeText(AddTransportActivity.this, "增加失败，请确保起点和终点信息无误", Toast.LENGTH_SHORT).show();
		        }
				if (result.error == SearchResult.ERRORNO.NO_ERROR) 
				{
					DrivingRouteLine routeLine = result.getRouteLines().get(0);
					RouteNode startNode = routeLine.getStarting();
					RouteNode endNode = routeLine.getTerminal();
					int time = routeLine.getDuration();
					int distance = routeLine.getDistance();
					putTransportData(startNode, endNode, "驾车", time, distance);
		        }
				else
				{
					mAddProgressDialog.dismiss();
				}
			}
		});
		
	}
	
	@Override
	public void onDestroy() 
	{
		mSearch.destroy();
		super.onDestroy();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == FROM_SIGHT_REQUEST && resultCode == RESULT_OK)
		{
			mFromSight.setLongitude(data.getDoubleExtra(Sight.LONGITUDE, 0.0));
			mFromSight.setLatitude(data.getDoubleExtra(Sight.LATITUDE, 0.0));
			mFromSight.setName(data.getStringExtra(Sight.NAME));
			
			mFromEditText.setText(mFromSight.getName());
		}
		else if (requestCode == TO_SIGHT_REQUEST && resultCode == RESULT_OK)
		{
			mToSight.setLongitude(data.getDoubleExtra(Sight.LONGITUDE, 0.0));
			mToSight.setLatitude(data.getDoubleExtra(Sight.LATITUDE, 0.0));
			mToSight.setName(data.getStringExtra(Sight.NAME));
			
			mToEditText.setText(mToSight.getName());
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	//获取当前位置
	private void getStartLocation()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mFromSight.setLongitude(sharedPreferences.getFloat(MainActivity.LONGITUDE, 0.0f));
		mFromSight.setLatitude(sharedPreferences.getFloat(MainActivity.LATITUDE, 0.0f));
		mFromSight.setName(sharedPreferences.getString(MainActivity.LOCATION_NAME, ""));
		
		mFromEditText.setText(mFromSight.getName());
	}
	
	private void getEndLocation()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		mToSight.setLongitude(sharedPreferences.getFloat(MainActivity.LONGITUDE, 0.0f));
		mToSight.setLatitude(sharedPreferences.getFloat(MainActivity.LATITUDE, 0.0f));
		mToSight.setName(sharedPreferences.getString(MainActivity.LOCATION_NAME, ""));
		
		mToEditText.setText(mToSight.getName());
	}
	
	//预览
	private void preview()
	{
		if (validate())
		{
			Intent intent = new Intent(this, TransportShowMapActivity.class);
			intent.putExtra(TransportShowMapActivity.SEARCH_TYPE, mIndex);
			
			if (mFromSight.getLongitude() != 0.0)
			{
				intent.putExtra(TransportShowMapActivity.START_LONGITUDE, mFromSight.getLongitude());
				intent.putExtra(TransportShowMapActivity.START_LATITUDE, mFromSight.getLatitude());
			}
			else
			{
				intent.putExtra(TransportShowMapActivity.START_CITY, mFromSight.getCity());
				intent.putExtra(TransportShowMapActivity.START_NAME, mFromSight.getName());
			}
			
			if (mToSight.getLongitude() != 0.0)
			{
				intent.putExtra(TransportShowMapActivity.END_LONGITUDE, mToSight.getLongitude());
				intent.putExtra(TransportShowMapActivity.END_LATITUDE, mToSight.getLatitude());
			}
			else
			{
				intent.putExtra(TransportShowMapActivity.END_CITY, mToSight.getCity());
				intent.putExtra(TransportShowMapActivity.END_NAME, mToSight.getName());
			}
			
			startActivity(intent);
		}
	}
	
	//改变按钮的背景
	private void changeButton(int currentPage)
	{
		if (mIndex == currentPage)
		{
			return;
		}
		
		switch (mIndex)
		{
		case 0:
			mCarButton.setBackgroundResource(R.drawable.gray_car);
			break;
		case 1:
			mBusButton.setBackgroundResource(R.drawable.gray_bus);
			break;
		case 2:
			mWalkButton.setBackgroundResource(R.drawable.gray_walk);
			break;
		}
		
		switch (currentPage)
		{
		case 0:
			mCarButton.setBackgroundResource(R.drawable.car);
			break;
		case 1:
			mBusButton.setBackgroundResource(R.drawable.bus);
			break;
		case 2:
			mWalkButton.setBackgroundResource(R.drawable.walk);
			break;
		}
	}

	//增加交通方式
	private void addTransport()
	{
		if (validate())
		{
			mAddProgressDialog.show();
			search();
		}
	}
	
	//验证起点和终点信息是否为空
	private boolean validate()
	{
		if (isEmpty(mFromSight))
		{
			Toast.makeText(AddTransportActivity.this, "请输入起点", Toast.LENGTH_SHORT).show();
			return false;
		}
		else if (isEmpty(mToSight))
		{
			Toast.makeText(AddTransportActivity.this, "请输入终点", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		return true;
	}
	
	//判断节点是否包含信息
	private boolean isEmpty(Sight sight)
	{
		if (!HandleUtil.isEmpty(sight.getName()))
		{
			return false;
		}
			
		return true;
	}

	private void search()
	{
		switch (mIndex)
		{
		case 0:
			DrivingRoutePlanOption option1 = new DrivingRoutePlanOption();
			option1.from(getStart()).to(getEnd());
			mSearch.drivingSearch(option1);
			break;
		case 1:
			TransitRoutePlanOption option2 = new TransitRoutePlanOption();
			option2.from(getStart()).to(getEnd());
			mSearch.transitSearch(option2);
			break;
		case 2:
			WalkingRoutePlanOption option3 = new WalkingRoutePlanOption();
			option3.from(getStart()).to(getEnd());
			mSearch.walkingSearch(option3);
			break;
		}
	}
	
	//获取起点
	private PlanNode getStart()
	{
		if (mFromSight.getLongitude() != 0.0)
		{
			LatLng latlng = new LatLng(mFromSight.getLatitude(), mFromSight.getLongitude());
			return PlanNode.withLocation(latlng);
		}
		else
		{
			if (!HandleUtil.isEmpty(mFromSight.getCity()))
			{
				return PlanNode.withCityNameAndPlaceName(mFromSight.getCity(), mFromSight.getName());
			}
			else
			{
				return PlanNode.withCityNameAndPlaceName("全国", mFromSight.getName());
			}
		}
	}
	
	//获取终点
	private PlanNode getEnd()
	{
		if (mToSight.getLongitude() != 0.0)
		{
			LatLng latlng = new LatLng(mToSight.getLatitude(), mToSight.getLongitude());
			return PlanNode.withLocation(latlng);
		}
		else
		{
			if (!HandleUtil.isEmpty(mToSight.getCity()))
			{
				return PlanNode.withCityNameAndPlaceName(mToSight.getCity(), mToSight.getName());
			}
			else
			{
				return PlanNode.withCityNameAndPlaceName("全国", mToSight.getName());
			}
		}
	}
	
	private void putTransportData(RouteNode startNode, RouteNode endNode, String description, int time, int distance)
	{
		Transport transport = new Transport();
		transport.setTime(time);
		transport.setDescription(description);
		transport.setDistance(distance);
		
		if (HandleUtil.isEmpty(startNode.getTitle()))
		{
			transport.setFromName(mFromSight.getName());
		}
		else
		{
			transport.setFromName(startNode.getTitle());
		}
		transport.setFromLongitude(startNode.getLocation().longitude);
		transport.setFromLatitude(startNode.getLocation().latitude);
		
		if (HandleUtil.isEmpty(endNode.getTitle()))
		{
			transport.setToName(mToSight.getName());
		}
		else
		{
			transport.setToName(endNode.getTitle());
		}
		transport.setToLongitude(endNode.getLocation().longitude);
		transport.setToLatitude(endNode.getLocation().latitude);

		String cookie = UserManager.getInstance(this).getCookie();
		String uri = HttpUrl.ROUTE + "/" + RouteManager.getInstance().getRoute(mRouteIndex).getId() + "/transport";
		HttpUtil.httpJson(mQueue, uri, Method.POST, cookie, transport.toJSON(), new AddTransportResponseListener());
	}
	
	private class AddTransportResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			Intent intent = new Intent();
			intent.putExtra(ADD_TRANSPORT, response);
			mAddProgressDialog.dismiss();
			setResult(RESULT_OK, intent);
			finish();
		}
		
		@Override
		public void onErrorResponse(String error) 
		{
			mAddProgressDialog.dismiss();
			Toast.makeText(AddTransportActivity.this, "增加失败", Toast.LENGTH_SHORT).show();
		}
	}
}
