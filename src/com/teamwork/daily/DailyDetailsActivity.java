package com.teamwork.daily;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

import com.android.volley.RequestQueue;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Daily;
import com.teamwork.model.Sight;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
import com.teamwork.utils.MyOnKeyListener;

public class DailyDetailsActivity extends Activity
{
	public static final String DAILY_INDEX = "daily_index";
	public static final String ROUTE_ID = "route_id";
	
	public static final int ADD_SIGHT = 99;
	public static final int FAVORITE_SIGHT = 100;
	
	private EditText mDailyDescriptionText;
	private ListView mSightsListView;
	private SightAdapter mSightAdapter;
	
	private int mIndex;
	private int mDeletePosition;
	private Daily mCurrentDaily;
	private String mRouteId;
	private boolean mIsChange = false;
	
	private RequestQueue mQueue;
	private ProgressDialog mSaveProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_daily_details);
		Intent intent = getIntent();
		mIndex = intent.getIntExtra(DAILY_INDEX, 0);
		mRouteId = intent.getStringExtra(ROUTE_ID);
		mCurrentDaily = DailyManager.getInstance().getDaily(mIndex);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		mSaveProgressDialog = HandleUtil.showProgressDialog(this, "正在保存...");
		
		showActionBar();
		
		mDailyDescriptionText = (EditText)findViewById(R.id.et_daily_description);
		mDailyDescriptionText.setText(mCurrentDaily.getRemark());
		mDailyDescriptionText.setOnKeyListener(new MyOnKeyListener());
		mDailyDescriptionText.setOnClickListener(new EditTextOnClickListener());
		mDailyDescriptionText.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) 
			{
				mIsChange = true;
				mCurrentDaily.setRemark(arg0.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
			
			@Override
			public void afterTextChanged(Editable arg0)
			{
			}
		});
		
		mSightsListView = (ListView)findViewById(R.id.lv_sights);
		mSightAdapter = new SightAdapter(mCurrentDaily.getSights());
		mSightsListView.setAdapter(mSightAdapter);
		registerForContextMenu(mSightsListView);
		
		Button favoriteButton = (Button)findViewById(R.id.btn_favorite);
		favoriteButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(DailyDetailsActivity.this, AllPlaceFavoriteActivity.class);
				startActivityForResult(intent, FAVORITE_SIGHT);
			}
		});
		
		Button addSightButton = (Button)findViewById(R.id.btn_add_sight);
		addSightButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(DailyDetailsActivity.this, SearchSightActivity.class);
				startActivityForResult(intent, ADD_SIGHT);
			}
		});
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				exit();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.daily_details);
 
        TextView saveText = (TextView)findViewById(R.id.tv_right);
        saveText.setText(R.string.save);
        saveText.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				save();
			}
		});
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		hideSoftInput();
	};

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) 
	{
		getMenuInflater().inflate(R.menu.sight_list_item_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		mDeletePosition = info.position;
		switch (item.getItemId())
		{
		case R.id.menu_item_delete_sight:
			deleteSight();
			break;
		}
		return super.onContextItemSelected(item);
	}
	
	private void deleteSight()
	{
		mCurrentDaily.getSights().remove(mDeletePosition);
		mSightAdapter.notifyDataSetChanged();
		mIsChange = true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == ADD_SIGHT && resultCode == RESULT_OK)
		{
			Sight sight = new Sight();
			sight.setName(data.getStringExtra(Sight.NAME));
			sight.setLongitude(data.getDoubleExtra(Sight.LONGITUDE, 0));
			sight.setLatitude(data.getDoubleExtra(Sight.LATITUDE, 0));
			mCurrentDaily.addSight(sight);
			mSightAdapter.notifyDataSetChanged();
			mIsChange = true;
		}
		else if (requestCode == FAVORITE_SIGHT && resultCode == RESULT_OK)
		{
			Sight sight = new Sight();
			sight.setName(data.getStringExtra(Sight.NAME));
			sight.setLongitude(data.getDoubleExtra(Sight.LONGITUDE, 0));
			sight.setLatitude(data.getDoubleExtra(Sight.LATITUDE, 0));
			mCurrentDaily.addSight(sight);
			mSightAdapter.notifyDataSetChanged();
			mIsChange = true;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void hideSoftInput()
	{
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
		if(inputMethodManager.isActive())
		{  
			inputMethodManager.hideSoftInputFromWindow(mDailyDescriptionText.getApplicationWindowToken(), 0 );
		}  
	}
	
	private class EditTextOnClickListener implements View.OnClickListener
	{
		@Override
		public void onClick(View v) 
		{
			EditText view = (EditText)v;
			view.setFocusable(true);
			view.setFocusableInTouchMode(true);
			view.requestFocus(); //获取焦点
			
			//弹出输入法
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
			inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED); 
		}
	}
	
	@Override
	public void onBackPressed() 
	{
		
		exit();
	}
	
	//处理退出
	private void exit()
	{
		if (mIsChange)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("是否保存对日程信息的修改？");
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() 
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					save();
				}
			});
			builder.setNegativeButton(R.string.give_up, new DialogInterface.OnClickListener() 
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					DailyManager.getInstance().reset(mIndex);
					finish();
				}
			});
		
			builder.create().show();
		}
		else
		{
			DailyManager.getInstance().reset(mIndex);
			finish();
		}
	}
	
	//保存修改
	private void save()
	{
		mSaveProgressDialog.show();
		String cookie = UserManager.getInstance(this).getCookie();
		String url = HttpUrl.ROUTE + "/" + mRouteId + "/daily/" + mCurrentDaily.getId();
		
		HttpUtil.httpJson(mQueue, url, Method.PUT, cookie, mCurrentDaily.toJSON(), new SaveDailyResponseListener());
		
	}
	
	private class SightAdapter extends ArrayAdapter<Sight>
	{
		public SightAdapter(ArrayList<Sight> routes)
		{
			super(DailyDetailsActivity.this, 0, routes);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.sight_list_item, null);
			}
			
			TextView numberTextView = (TextView)convertView.findViewById(R.id.tv_number);
			numberTextView.setText(String.valueOf(position+1));
			
			Sight sight = getItem(position);
			TextView nameTextView = (TextView)convertView.findViewById(R.id.tv_sight_name);
			nameTextView.setText(sight.getName());
			
			return convertView;
		}
	}
	
	private class SaveDailyResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mSaveProgressDialog.dismiss();
			Intent intent = new Intent();
			intent.putExtra(DAILY_INDEX, mIndex);
			setResult(RESULT_OK, intent);
			finish();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mSaveProgressDialog.dismiss();
			Toast.makeText(DailyDetailsActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}

}
