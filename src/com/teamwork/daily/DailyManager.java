package com.teamwork.daily;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.teamwork.model.Daily;

public class DailyManager 
{
	private ArrayList<Daily> mDailys;
	private Daily mCurrentDaily;
	
	private static DailyManager sDailyManager;
	
	private DailyManager()
	{
		mDailys = new ArrayList<Daily>();
	}
	
	public static DailyManager getInstance()
	{
		if (sDailyManager == null)
		{
			sDailyManager = new DailyManager();
		}
		
		return sDailyManager;
	}
	
	public void clear()
	{
		mDailys.clear();
	}
	
	public void addDaily(Daily daily)
	{
		mDailys.add(daily);
	}
	
	public Daily getDaily(int index)
	{
		Daily daily = mDailys.get(index);
		mCurrentDaily = daily.clone();
		return daily;

	}
	
	public void reset(int index)
	{
		Daily temp = mDailys.get(index);
		temp.setRemark(mCurrentDaily.getRemark());
		temp.setSights(mCurrentDaily.getSights());
	}
	
	public void removeDaily(int index)
	{
		mDailys.remove(index);
	}
	
	public ArrayList<Daily> getDailys()
	{
		return  mDailys;
	}

	public void parseJsonStringToDailys(String jsonString)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArray = jsonObject.getJSONArray("daily");
			for (int i = 0; i < jsonArray.length(); i++)
			{
				mDailys.add(new Daily(jsonArray.getJSONObject(i)));
			}
			
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
}
