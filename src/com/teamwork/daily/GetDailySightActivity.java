package com.teamwork.daily;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Daily;
import com.teamwork.model.Sight;

public class GetDailySightActivity extends Activity 
{
	
	private ListView mListView;
	private SightAdapter mSightAdapter;
	private ArrayList<String> mGroupKey = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_daily_sight);
	
		showActionBar();
		
		mListView = (ListView)findViewById(R.id.lv_sights);
		mSightAdapter = new SightAdapter(new ArrayList<Sight>());
		mListView.setAdapter(mSightAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) 
			{
				Sight sight = mSightAdapter.getItem(position);
				
				Intent intent = new Intent();
				intent.putExtra(Sight.NAME, sight.getName());
				intent.putExtra(Sight.LONGITUDE, sight.getLongitude());
				intent.putExtra(Sight.LATITUDE, sight.getLatitude());
				setResult(RESULT_OK, intent);
				finish();
			}
		});
		
		getAllAddedSight();
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.already_add_sight);
        
	}

	//获取所有已添加的景点
	private void getAllAddedSight()
	{
		DailyManager dailyManager = DailyManager.getInstance();
		ArrayList<Daily> dailys = dailyManager.getDailys();
		Sight sight;
		for (int i = 0; i < dailys.size(); i++)
		{
			sight = new Sight();
			String s = "第" + (i+1) + "天";
			sight.setName(s);
			mSightAdapter.add(sight);
			mGroupKey.add(s);
			for (Sight temp : dailys.get(i).getSights())
			{
				mSightAdapter.add(temp);
			}
		}
		
		mSightAdapter.notifyDataSetChanged();
	}
	
	private class SightAdapter extends ArrayAdapter<Sight>
	{
		public SightAdapter(ArrayList<Sight> sights)
		{
			super(GetDailySightActivity.this, 0, sights);
		}
		
		@Override
		public boolean isEnabled(int position)
		{
			Sight sight = getItem(position);
			if (mGroupKey.contains(sight.getName()))
			{
				return false;
			}
			
			return super.isEnabled(position);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			Sight sight = getItem(position);
			if (mGroupKey.contains(sight.getName()))
			{
				convertView = getLayoutInflater().inflate(R.layout.sight_list_group, null);
				
				TextView textView = (TextView)convertView.findViewById(R.id.tv_group);
				textView.setText(sight.getName());
			}
			else
			{
				convertView = getLayoutInflater().inflate(R.layout.sight_list_item, null);
				
				TextView sightName = (TextView)convertView.findViewById(R.id.tv_sight_name);
				sightName.setText(sight.getName());
			}
			
			return convertView;
		}
	}
}
