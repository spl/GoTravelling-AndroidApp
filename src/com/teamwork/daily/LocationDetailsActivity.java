package com.teamwork.daily;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Sight;

public class LocationDetailsActivity extends Activity
{
	private PoiSearch mPoiSearch = null;
			
	private String mUid;
	private String mName;
	
	private WebView mWebView;
	private Button mShowInMapButton;
	
	private PoiDetailResult mPoiDetailResult;
	
	@SuppressLint("SetJavaScriptEnabled") 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location_details);
		
		Intent intent = getIntent();
		mUid = intent.getStringExtra(Sight.UID);
		mName = intent.getStringExtra(Sight.NAME);
		
		showActionBar();
		
		final ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar);
		progressBar.setMax(100);
		
		mWebView = (WebView)findViewById(R.id.web_view);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebViewClient(new WebViewClient()
		{
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				return false;
			}
		});
		mWebView.setWebChromeClient(new WebChromeClient()
		{
			@Override
			public void onProgressChanged(WebView view, int progress)
			{
				if (progress == 100)
				{
					progressBar.setVisibility(View.INVISIBLE);
				}
				else
				{
					progressBar.setVisibility(View.VISIBLE);
					progressBar.setProgress(progress);
				}
			}
		});
		
		mShowInMapButton = (Button)findViewById(R.id.btn_show_in_map);
		mShowInMapButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(LocationDetailsActivity.this, SightShowMapActivity.class);
				intent.putExtra(Sight.NAME, mPoiDetailResult.getName());
				intent.putExtra(Sight.LONGITUDE, mPoiDetailResult.getLocation().longitude);
				intent.putExtra(Sight.LATITUDE, mPoiDetailResult.getLocation().latitude);
				startActivity(intent);
			}
		});
		
		mPoiSearch = PoiSearch.newInstance();
		mPoiSearch.setOnGetPoiSearchResultListener(new OnGetPoiSearchResultListener() 
		{
			
			@Override
			public void onGetPoiResult(PoiResult result) 
			{
				
			}
			
			@Override
			public void onGetPoiDetailResult(PoiDetailResult result)
			{
				mWebView.loadUrl(result.getDetailUrl());
				mPoiDetailResult = result;
				mShowInMapButton.setEnabled(true);
			}
		});
		
		mPoiSearch.searchPoiDetail((new PoiDetailSearchOption()).poiUid(mUid));

	}
	
	@Override
	protected void onDestroy()
	{
		mPoiSearch.destroy();
		super.onDestroy();
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(mName);
	}
}
