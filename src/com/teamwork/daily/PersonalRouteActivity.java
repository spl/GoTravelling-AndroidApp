package com.teamwork.daily;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Route;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class PersonalRouteActivity extends Activity 
{
	public static final String ROUTE_POSITION = "route_position";

	private ListView mRouteListView;
	private RouteAdapter mRouteAdapter;
	
	private RequestQueue mQueue;
	
	private ProgressDialog mGetProgressDialog;
	private ProgressDialog mAddProgressDialog;
	private ProgressDialog mDeleteProgressDialog;
	
	private RouteManager mRouteManager;
	
	private int mCurrentPosition;
	private Route mDeleteRoute;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personal_route);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		mRouteManager = RouteManager.getInstance();
		
		mGetProgressDialog = HandleUtil.showProgressDialog(this, "正在获取...");
		mAddProgressDialog = HandleUtil.showProgressDialog(this, "正在新增路线...");
		mDeleteProgressDialog = HandleUtil.showProgressDialog(this, "正在删除...");
		
		showActionBar();
		
		mRouteListView = (ListView)findViewById(R.id.lv_routes);
		mRouteListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
			{
				Intent intent = new Intent(PersonalRouteActivity.this, RouteDetailsActivity.class);
				intent.putExtra(ROUTE_POSITION, position);
				startActivity(intent);
			}
		});
		
		if (!mRouteManager.getState())
		{
			getPersonalRoutes();
		}
		else
		{
			initListView();
		}
		
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		if (mRouteAdapter != null)
		{
			mRouteAdapter.notifyDataSetChanged();
		}
	}
	
	//初始化listView
	private void initListView()
	{
		mRouteAdapter = new RouteAdapter(mRouteManager.getRoutes());
		if (mRouteAdapter.getCount() == 0)
		{
			Toast.makeText(this, "当前路线列表为空", Toast.LENGTH_SHORT).show();
		}
		mRouteListView.setAdapter(mRouteAdapter);
		registerForContextMenu(mRouteListView);
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.my_route);
        
        Button addButton = (Button)findViewById(R.id.btn_right);
        addButton.setVisibility(View.VISIBLE);
        addButton.setBackgroundResource(R.drawable.btn_add);
        addButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				addRoute();
			}
		}); 
	}
	
	//增加路线
	private void addRoute()
	{
		showDialog();
	}
	
	//增加路线时显示的对话框
	private void showDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(PersonalRouteActivity.this);
		builder.setTitle("新增路线");
		LinearLayout ll = (LinearLayout)getLayoutInflater().inflate(R.layout.edit_text, null);
		final EditText routeNameEditText = (EditText)ll.findViewById(R.id.edit_text);
		routeNameEditText.setHint(R.string.route_name_hint);
		builder.setView(ll);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String routeName = routeNameEditText.getText().toString();
				if (HandleUtil.isEmpty(routeName))
				{
					routeName = "未命名路线";
				}
				
				putAddRoute(routeName);

			}
		});
		builder.setNegativeButton(R.string.cancel, null);
		builder.create().show();
	}
	
	//向后台发出新建路线的请求
	private void putAddRoute(String routeName)
	{
		mAddProgressDialog.show();
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(Route.NAME, routeName);
		params.put(UserManager.COOKIE, UserManager.getInstance(this).getCookie());
		
		HttpUtil.httpPostForJson(mQueue, HttpUrl.ROUTE, Method.POST, false, params, new AddRouteResponseListener());
	}
	
	//获取路线列表
	private void getPersonalRoutes()
	{
		mGetProgressDialog.show();
		String cookie = UserManager.getInstance(this).getCookie();
		
		HttpUtil.httpJsonGetForArray(mQueue, HttpUrl.ROUTE, cookie, "type=mine", new GetRouteResponseListener());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) 
	{
		getMenuInflater().inflate(R.menu.route_list_item_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		mCurrentPosition = info.position;
		mDeleteRoute = mRouteAdapter.getItem(mCurrentPosition);
		switch (item.getItemId())
		{
		case R.id.menu_item_delete_route:
			mDeleteProgressDialog.show();
			deleteRoute();
			break;
		}
		return super.onContextItemSelected(item);
	}
	
	//删除路线
	private void deleteRoute()
	{
		String cookie = UserManager.getInstance(this).getCookie();
			
		String uri = HttpUrl.ROUTE + "/" + mDeleteRoute.getId();
		HttpUtil.httpGet(mQueue, uri, Method.DELETE, cookie, new DeleteRouteResponseListener());
	}
		
	private class RouteAdapter extends ArrayAdapter<Route>
	{
		public RouteAdapter(ArrayList<Route> routes)
		{
			super(PersonalRouteActivity.this, 0, routes);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.personal_route_item, null);
			}
			
			Route route = getItem(position);
			TextView nameTextView = (TextView)convertView.findViewById(R.id.tv_route_name);
			nameTextView.setText(route.getName());
			
			return convertView;
		}
	}
	
	private class AddRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			JSONObject jsonObject = null;
			try 
			{
				jsonObject = new JSONObject(response);
			}
			catch (JSONException e) 
			{

				e.printStackTrace();
			}
			Route route = new Route(jsonObject);
			mRouteManager.addRoute(route);
			
			mRouteAdapter.notifyDataSetChanged();
			mAddProgressDialog.dismiss();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mAddProgressDialog.dismiss();
			Toast.makeText(PersonalRouteActivity.this, "新增路线失败", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class DeleteRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mRouteManager.removeRoute(mCurrentPosition);
			mRouteAdapter.notifyDataSetChanged();
			mDeleteProgressDialog.dismiss();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mDeleteProgressDialog.dismiss();
			Toast.makeText(PersonalRouteActivity.this, "删除路线失败", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class GetRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mRouteManager.parseJsonStringToRoutes(response);
			mRouteManager.setState(true);
			mGetProgressDialog.dismiss();
			initListView();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mGetProgressDialog.dismiss();
			Toast.makeText(PersonalRouteActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
}
