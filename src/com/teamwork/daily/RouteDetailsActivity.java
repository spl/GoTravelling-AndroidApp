package com.teamwork.daily;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Daily;
import com.teamwork.model.Route;
import com.teamwork.model.Transport;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
import com.teamwork.utils.MyOnKeyListener;

public class RouteDetailsActivity extends Activity 
{
	private static final int ADD_TRANSPORT_REQUEST = 100;
	private static final int DAILY_INFO = 101;
	
	private EditText mRouteNameText;
	private EditText mRouteDescriptionText;
	private RadioGroup mRadioGroup;
	private RadioButton mRadioButton1;
	private RadioButton mRadioButton2;
	private RadioButton mRadioButton3;
	private Button mDailyButton;
	private Button mTransportButton;
	
	private RelativeLayout mDailyLayout;
	private LinearLayout mDailyList;
	
	private RelativeLayout mTransportLayout;
	private LinearLayout mTransportList;
	
	private RequestQueue mQueue;
	
	private int mRoutePosition;
	private Route mRoute;
	
	//当前获取焦点弹出输入法的view
	private View mWindowToken;
	
	private boolean mDailyOpen = false;
	
	private boolean mTransportOpen = false;
	
	private ProgressDialog mGetProgressDialog;
	private ProgressDialog mSaveProgressDialog;
	private ProgressDialog mAddProgressDialog;
	private ProgressDialog mDeleteProgressDialog;
	
	private ArrayList<Daily> mDailys;
	private ArrayList<Transport> mTransports;
	
	private int mDeleteDailyIndex;
	private int mDeleteTransportIndex;
	
	//记录是否有做修改
	private boolean mIsChange = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route_details);
		
		Intent intent = getIntent();
		mRoutePosition = intent.getIntExtra(PersonalRouteActivity.ROUTE_POSITION, 0);
		mRoute = RouteManager.getInstance().getRoute(mRoutePosition);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		mGetProgressDialog = HandleUtil.showProgressDialog(this, "正在获取该路线的详情信息...");
		mSaveProgressDialog = HandleUtil.showProgressDialog(this, "正在保存...");
		mAddProgressDialog = HandleUtil.showProgressDialog(this, "正在增加...");
		mDeleteProgressDialog = HandleUtil.showProgressDialog(this, "正在删除...");
		
		showActionBar();
		
		mRouteNameText = (EditText)findViewById(R.id.et_route_name);
		mRouteNameText.setOnKeyListener(new MyOnKeyListener());
		mRouteNameText.setText(mRoute.getName());
		mRouteNameText.setOnClickListener(new EditTextOnClickListener());
		mRouteNameText.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) 
			{
				mRoute.setName(arg0.toString());
				mIsChange = true;
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
			
			@Override
			public void afterTextChanged(Editable arg0)
			{
			}
		});
		
		mRouteDescriptionText = (EditText)findViewById(R.id.et_route_description);
		mRouteDescriptionText.setOnClickListener(new EditTextOnClickListener());
		mRouteDescriptionText.setOnKeyListener(new MyOnKeyListener());
		mRouteDescriptionText.setText(mRoute.getDescription());
		mRouteDescriptionText.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) 
			{
				mRoute.setDescription(arg0.toString());
				mIsChange = true;
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
			}
			
			@Override
			public void afterTextChanged(Editable arg0)
			{
			}
		});
		
		mRadioGroup = (RadioGroup)findViewById(R.id.rg_route_status);
		mRadioButton1 = (RadioButton)findViewById(R.id.rb_status_non_start);
		mRadioButton2 = (RadioButton)findViewById(R.id.rb_status_underway);
		mRadioButton3 = (RadioButton)findViewById(R.id.rb_status_completed);
		
		initRadioButton();
		
		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				mIsChange = true;
				switch (checkedId)
				{
				case R.id.rb_status_non_start:
					mRoute.setStatus("planning");
					break;
				case R.id.rb_status_underway:
					mRoute.setStatus("travelling");
					break;
				case R.id.rb_status_completed:
					mRoute.setStatus("finished");
					break;
				}
			}
		});
		
		mDailyButton = (Button)findViewById(R.id.btn_daily);
		mDailyButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				if (mDailyOpen)
				{
					mDailyButton.setBackgroundResource(R.drawable.expand);
					mDailyOpen = false;
					mDailyLayout.setVisibility(View.GONE);
				}
				else
				{
					mDailyButton.setBackgroundResource(R.drawable.collapse);
					mDailyOpen = true;
					mDailyLayout.setVisibility(View.VISIBLE);
					if (mTransportOpen)
					{
						mTransportButton.setBackgroundResource(R.drawable.expand);
						mTransportOpen = false;
						mTransportLayout.setVisibility(View.GONE);
					}
				}
			}
		});
		
		mTransportButton = (Button)findViewById(R.id.btn_transport);
		mTransportButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				if (mTransportOpen)
				{
					mTransportButton.setBackgroundResource(R.drawable.expand);
					mTransportOpen = false;
					mTransportLayout.setVisibility(View.GONE);
				}
				else
				{
					mTransportButton.setBackgroundResource(R.drawable.collapse);
					mTransportOpen = true;
					mTransportLayout.setVisibility(View.VISIBLE);
					if (mDailyOpen)
					{
						mDailyButton.setBackgroundResource(R.drawable.expand);
						mDailyOpen = false;
						mDailyLayout.setVisibility(View.GONE);
					}
				}
			}
		});
		
		getRouteDetails();
		
		initDaily();
		initTransport();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		hideSoftInput();
	};
	
	@Override
	protected void onDestroy()
	{
		DailyManager.getInstance().clear();
		TransportManager.getInstance().clear();
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() 
	{
		
		exit();
	}
	
	//处理退出
	private void exit()
	{
		if (mIsChange)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("是否保存对路线信息的修改？");
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() 
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					save();
				}
			});
			builder.setNegativeButton(R.string.give_up, new DialogInterface.OnClickListener() 
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					RouteManager.getInstance().reset(mRoutePosition);
					finish();
				}
			});
		
			builder.create().show();
		}
		else
		{
			RouteManager.getInstance().reset(mRoutePosition);
			finish();
		}
	}
	
	private void getRouteDetails()
	{
		mGetProgressDialog.show();
		
		String cookie = UserManager.getInstance(this).getCookie();
		String uri = HttpUrl.ROUTE + "/" + mRoute.getId();
		HttpUtil.httpGet(mQueue, uri, Method.GET, cookie, new RouteDetailsResponseListener());
	}
	
	private void initRadioButton()
	{
		String status = mRoute.getStatus();
		if ("planning".equals(status))
		{
			mRadioButton1.setChecked(true);
		}
		else if ("travelling".equals(status))
		{
			mRadioButton2.setChecked(true);
		}
		else
		{
			mRadioButton3.setChecked(true);
		}
	}
	
	private void initDaily()
	{
		mDailyLayout = (RelativeLayout)findViewById(R.id.rl_daily);
		mDailyList = (LinearLayout)findViewById(R.id.ll_daily_list);
		
		Button addButton = (Button)findViewById(R.id.btn_daily_add);
		addButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				showDialog();
			}
		});
	}
	
	private View addDailyView(final int i, String description)
	{
		View v = getLayoutInflater().inflate(R.layout.daily_list_item, null);
		TextView dailyText = (TextView)v.findViewById(R.id.tv_daily);
		String text = "第" + (i+1) + "天";
		dailyText.setText(text);
		TextView descriptionText = (TextView)v.findViewById(R.id.tv_daily_description);
		descriptionText.setText(description);
		View ll = v.findViewById(R.id.ll_daily_list);
		ll.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(RouteDetailsActivity.this, DailyDetailsActivity.class);
				intent.putExtra(DailyDetailsActivity.DAILY_INDEX, i);
				intent.putExtra(DailyDetailsActivity.ROUTE_ID, mRoute.getId());
				startActivityForResult(intent, DAILY_INFO);
			}
		});
		ll.setOnLongClickListener(new View.OnLongClickListener() 
		{
			@Override
			public boolean onLongClick(View v)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(RouteDetailsActivity.this);
				builder.setTitle("是否删除该日程？");
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						mDeleteDailyIndex = i;
						deleteDaily();
					}
				});
				builder.setNegativeButton(R.string.cancel, null);
				builder.create().show();
				return true;
			}
		});
		return v;
	}
	
	private void initTransport()
	{
		mTransportLayout = (RelativeLayout)findViewById(R.id.rl_transport);
		mTransportList = (LinearLayout)findViewById(R.id.ll_transport_list);
		
		Button addButton = (Button)findViewById(R.id.btn_transport_add);
		addButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(RouteDetailsActivity.this, AddTransportActivity.class);
				intent.putExtra(AddTransportActivity.ROUTE_INDEX, mRoutePosition);
				startActivityForResult(intent, ADD_TRANSPORT_REQUEST);
			}
		});
	}
	
	private View addTransportView(final int i, Transport transport)
	{
		View v = getLayoutInflater().inflate(R.layout.transport_list_item, null);
		TextView fromText = (TextView)v.findViewById(R.id.tv_from);
		fromText.setText(transport.getFromName());
		TextView toText = (TextView)v.findViewById(R.id.tv_to);
		toText.setText(transport.getToName());
		v.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(RouteDetailsActivity.this, TransportDetailsActivity.class);
				intent.putExtra(TransportDetailsActivity.TRANSPORT_INDEX, i);
				startActivity(intent);
			}
		});
		v.setOnLongClickListener(new View.OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v) 
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(RouteDetailsActivity.this);
				builder.setTitle("是否删除该交通方式？");
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						mDeleteTransportIndex = i;
						deleteTransport();
					}
				});
				builder.setNegativeButton(R.string.cancel, null);
				builder.create().show();
				return true;
			}
		});
		
		return v;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == ADD_TRANSPORT_REQUEST && resultCode == RESULT_OK)
		{
			String jsonString = data.getStringExtra(AddTransportActivity.ADD_TRANSPORT);
			try
			{
				JSONObject jsonObject = new JSONObject(jsonString);
				Transport transport = new Transport(jsonObject);
				int count = mTransports.size();
				mTransportList.addView(addTransportView(count, transport));
				mTransports.add(transport);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		else if (requestCode == DAILY_INFO && resultCode == RESULT_OK)
		{
			int index = data.getIntExtra(DailyDetailsActivity.DAILY_INDEX, 0);
			Daily daily = DailyManager.getInstance().getDaily(index);
			View v = mDailyList.getChildAt(index);
			TextView text = (TextView)v.findViewById(R.id.tv_daily_description);
			text.setText(daily.getRemark());
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	//删除日程
	private void deleteDaily()
	{
		mDeleteProgressDialog.show();
		String cookie = UserManager.getInstance(this).getCookie();
		String url = HttpUrl.ROUTE + "/" + mRoute.getId() + "/daily/" + mDailys.get(mDeleteDailyIndex).getId();
		HttpUtil.httpGet(mQueue, url, Method.DELETE, cookie, new DeleteDailyResponseListener());
	}
	
	//删除交通方式
	private void deleteTransport()
	{
		mDeleteProgressDialog.show();
		String cookie = UserManager.getInstance(this).getCookie();
		String url = HttpUrl.ROUTE + "/" + mRoute.getId() + "/transport/" + mTransports.get(mDeleteTransportIndex).getId();
		HttpUtil.httpGet(mQueue, url, Method.DELETE, cookie, new DeleteTransportResponseListener());
	}
	
	//增加日程时显示的对话框
	private void showDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(RouteDetailsActivity.this);
		builder.setTitle("新增日程");
		LinearLayout ll = (LinearLayout)getLayoutInflater().inflate(R.layout.edit_text, null);
		final EditText DailyRemarkEditText = (EditText)ll.findViewById(R.id.edit_text);
		DailyRemarkEditText.setHint(R.string.daily_remark_hint);
		builder.setView(ll);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String dailyRemark = DailyRemarkEditText.getText().toString();
				if (HandleUtil.isEmpty(dailyRemark))
				{
					dailyRemark = "日程简介";
				}
				putAddDaily(dailyRemark);
			}
		});
		builder.setNegativeButton(R.string.cancel, null);
		builder.create().show();
	}
	
	private void putAddDaily(String dailyRemark)
	{
		mAddProgressDialog.show();        
			
		Map<String, String> params = new HashMap<String, String>();
		params.put(Daily.REMARK, dailyRemark);
		params.put(UserManager.COOKIE, UserManager.getInstance(this).getCookie());
		
		String uri = HttpUrl.ROUTE + "/" + mRoute.getId() + "/daily";
		HttpUtil.httpPostForJson(mQueue, uri, Method.POST, false, params, new AddDailyResponseListener());
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				exit();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.route_details);
        
        TextView saveText = (TextView)findViewById(R.id.tv_right);
        saveText.setText(R.string.save);
        saveText.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				save();
			}
		});
	}
	
	//保存修改
	private void save()
	{
		mSaveProgressDialog.show();
		String cookie = UserManager.getInstance(this).getCookie();
		String url = HttpUrl.ROUTE + "/" + mRoute.getId();
		
		HttpUtil.httpJson(mQueue, url, Method.PUT, cookie, mRoute.toJSON(), new SaveRouteResponseListener());
		
	}
	
	//隐藏输入键盘
	private void hideSoftInput()
	{
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
		if(inputMethodManager.isActive() && mWindowToken != null)
		{  
			inputMethodManager.hideSoftInputFromWindow(mWindowToken.getApplicationWindowToken(), 0 );
			  
		}
	}
		
	private class EditTextOnClickListener implements View.OnClickListener
	{
		@Override
		public void onClick(View v) 
		{
			EditText view = (EditText)v;
			view.setFocusable(true);
			view.setFocusableInTouchMode(true);
			view.requestFocus(); //获取焦点	
			//弹出输入法
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
			inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED); 
			mWindowToken = view;
		}
	}
	
	private class RouteDetailsResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			DailyManager.getInstance().parseJsonStringToDailys(response);
			TransportManager.getInstance().parseJsonStringToTransports(response);
			mGetProgressDialog.dismiss();
			mDailys = DailyManager.getInstance().getDailys();
			for (int i = 0; i < mDailys.size(); i++)
			{
				mDailyList.addView(addDailyView(i, mDailys.get(i).getRemark()));
			}
			mTransports = TransportManager.getInstance().getTransports();
			for (int i = 0; i < mTransports.size(); i++)
			{
				
				mTransportList.addView(addTransportView(i, mTransports.get(i)));
			}
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mGetProgressDialog.dismiss();
			Toast.makeText(RouteDetailsActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class SaveRouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mSaveProgressDialog.dismiss();
			finish();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mSaveProgressDialog.dismiss();
			Toast.makeText(RouteDetailsActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class AddDailyResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mAddProgressDialog.dismiss();

			try 
			{
				JSONObject jsonObject = new JSONObject(response);
				Daily daily = new Daily(jsonObject);
				int count = mDailys.size();
				mDailyList.addView(addDailyView(count, daily.getRemark()));
				mDailys.add(daily);
			} 
			catch (JSONException e)
			{

				e.printStackTrace();
			}
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mAddProgressDialog.dismiss();
			Toast.makeText(RouteDetailsActivity.this, "新增日程失败", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class DeleteDailyResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mDailys.remove(mDeleteDailyIndex);
			mDailyList.removeAllViews();
			for (int i = 0; i < mDailys.size(); i++)
			{
				mDailyList.addView(addDailyView(i, mDailys.get(i).getRemark()));
			}
			mDeleteProgressDialog.dismiss();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mDeleteProgressDialog.dismiss();
			Toast.makeText(RouteDetailsActivity.this, "删除日程失败", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class DeleteTransportResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mTransports.remove(mDeleteTransportIndex);
			mTransportList.removeAllViews();
			for (int i = 0; i < mTransports.size(); i++)
			{
				mTransportList.addView(addTransportView(i, mTransports.get(i)));
			}
			mDeleteProgressDialog.dismiss();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mDeleteProgressDialog.dismiss();
			Toast.makeText(RouteDetailsActivity.this, "删除日程失败", Toast.LENGTH_SHORT).show();
		}
	}
}
