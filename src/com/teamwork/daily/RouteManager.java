package com.teamwork.daily;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.teamwork.model.Route;

public class RouteManager 
{
	private ArrayList<Route> mRoutes;
	private boolean mState;
	private Route mCurrentRoute;
	
	private static RouteManager sRouteManager;
	
	private RouteManager()
	{
		mRoutes = new ArrayList<Route>();
		mState = false;
	}
	
	public static RouteManager getInstance()
	{
		if (sRouteManager == null)
		{
			sRouteManager = new RouteManager();
		}
		
		return sRouteManager;
	}
	
	public void setState(boolean state)
	{
		mState = state;
	}
	
	public boolean getState()
	{
		return mState;
	}
	
	public void clear()
	{
		mRoutes.clear();
	}
	
	public void addRoute(Route route)
	{
		mRoutes.add(0, route);
	}
	
	public Route getRoute(int index)
	{
		Route temp = mRoutes.get(index);
		mCurrentRoute = temp.clone();
		return temp;
	}
	
	public void reset(int index)
	{
		Route temp = mRoutes.get(index);
		temp.setName(mCurrentRoute.getName());
		temp.setDescription(mCurrentRoute.getDescription());
		temp.setStatus(mCurrentRoute.getStatus());
	}
	
	public void removeRoute(int index)
	{
		mRoutes.remove(index);
	}
	
	public ArrayList<Route> getRoutes()
	{
		return mRoutes;
	}
	
	public void parseJsonStringToRoutes(String jsonString)
	{
		try
		{
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); i++)
			{
				mRoutes.add(new Route(jsonArray.getJSONObject(i)));
			}
			
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
}
