package com.teamwork.daily;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Sight;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.MyOnKeyListener;

public class SearchSightActivity extends Activity 
{
	private AutoCompleteTextView mSearchText;
	private ArrayAdapter<String> mSuggestionAdapter = null;
	private ArrayList<Sight> mSights = new ArrayList<Sight>();
	private ListView mSightListView;
	private SightAdapter mSightAdapter;
	private ArrayList<String> mListName = new ArrayList<String>();
	
	private boolean mCanShow = true;
	
	//在线建议查询
	private SuggestionSearch mSuggestionSearch = null;
	//poi查询
	private PoiSearch mPoiSearch = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_sight);
		
		mSuggestionSearch = SuggestionSearch.newInstance();
		mSuggestionSearch.setOnGetSuggestionResultListener(new OnGetSuggestionResultListener() 
		{
			 public void onGetSuggestionResult(SuggestionResult res) 
			 { 
				 if (res == null || res.getAllSuggestions() == null) //未找到相关结果  
				 {
					 Toast.makeText(SearchSightActivity.this, "未找到相关结果", Toast.LENGTH_SHORT).show();
					 return;       
			     }  
				 //获取在线建议检索结果  
				 mSuggestionAdapter.clear();
				 for (SuggestionResult.SuggestionInfo info : res.getAllSuggestions()) 
				 {
					 if (!HandleUtil.isEmpty(info.city))
						 mSuggestionAdapter.add(info.key + "(" + info.city + ")");
				 }
				mSuggestionAdapter.notifyDataSetChanged();
				//确保第一次输入时刷新adapter后显示数据
				if (!mSearchText.isPopupShowing() && mCanShow)
				{
					mSearchText.setText(mSearchText.getText().toString());
					mSearchText.setSelection(mSearchText.getText().length());
					mCanShow = false;
				}
				
			 } 
		});
		
		mPoiSearch = PoiSearch.newInstance();
		mPoiSearch.setOnGetPoiSearchResultListener(new OnGetPoiSearchResultListener() 
		{
			@Override
			public void onGetPoiResult(PoiResult result)
			{
				mSightAdapter.clear();
				if (result == null || result.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) 
				{
					Toast.makeText(SearchSightActivity.this, "未找到结果", Toast.LENGTH_SHORT).show();
					return;
				}
				//成功返回结果
				if (result.error == SearchResult.ERRORNO.NO_ERROR)
				{
					Sight sight;
					for (PoiInfo poiInfo : result.getAllPoi())
					{
						if (!mListName.contains(poiInfo.name))
						{
							sight = new Sight();
							sight.setName(poiInfo.name);
							sight.setCity(poiInfo.city);
							sight.setAddress(poiInfo.address);
							sight.setLongitude(poiInfo.location.longitude);
							sight.setLatitude(poiInfo.location.latitude);
							sight.setUid(poiInfo.uid);
						
							mSightAdapter.add(sight);
							mListName.add(poiInfo.name);
						}
					}
					mSightAdapter.notifyDataSetChanged();
					return;
				}
			}
			
			@Override
			public void onGetPoiDetailResult(PoiDetailResult result) 
			{
				
			}
		});
		
		mSightAdapter = new SightAdapter(mSights);
		
		mSightListView = (ListView)findViewById(R.id.lv_locations);
		mSightListView.setAdapter(mSightAdapter);
		mSightListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id) 
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(SearchSightActivity.this);
				View v = getLayoutInflater().inflate(R.layout.sight_list_menu, null);
				builder.setView(v);
				final AlertDialog dialog = builder.create();
				TextView addText = (TextView)v.findViewById(R.id.tv_add_sight);
				addText.setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						dialog.dismiss();
						Sight sight = mSightAdapter.getItem(position);
						Intent intent = new Intent();
						intent.putExtra(Sight.NAME, sight.getName());
						intent.putExtra(Sight.LONGITUDE, sight.getLongitude());
						intent.putExtra(Sight.LATITUDE, sight.getLatitude());
						setResult(RESULT_OK, intent);
						finish();
					}
				});
				TextView detailsText = (TextView)v.findViewById(R.id.tv_sight_details);
				detailsText.setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						dialog.dismiss();
						Sight sight = mSightAdapter.getItem(position);
						Intent intent = new Intent(SearchSightActivity.this, SightShowMapActivity.class);
						intent.putExtra(Sight.NAME, sight.getName());
						intent.putExtra(Sight.LONGITUDE, sight.getLongitude());
						intent.putExtra(Sight.LATITUDE, sight.getLatitude());
						startActivity(intent);
					}
				});
				
				dialog.show();
			}
		});
		
		mSearchText = (AutoCompleteTextView)findViewById(R.id.tv_search);
		mSuggestionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line);
		mSearchText.setAdapter(mSuggestionAdapter);
		mSearchText.setOnKeyListener(new MyOnKeyListener());
		mSearchText.addTextChangedListener(new TextWatcher()
		{	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (s.length() <= 0) 
				{
					return;
				}
				//使用建议搜索服务获取建议列表，结果在onSuggestionResult()中更新  
				mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())  
				    .keyword(s.toString())
				    .city("全国"));
				mCanShow = true;
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) 
			{
				
			}
		});
		
		mSearchText.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView textView = (TextView)view;
				String searchText = textView.getText().toString();
				String[] array = searchText.split("\\(");
				String city = array[array.length-1];
				city = city.substring(0, city.length()-1);
				String spot = array[0];
				mPoiSearch.searchInCity((new PoiCitySearchOption())
						.city(city)
						.keyword(spot)
						.pageCapacity(20));
				mSearchText.dismissDropDown();
				mCanShow = false;
				//隐藏输入键盘
				HandleUtil.hideSoftInput(mSearchText);
				mListName.clear();
			}
		});
	}
	
	@Override
	protected void onDestroy()
	{
		mSuggestionSearch.destroy();
		mPoiSearch.destroy();
		super.onDestroy();
	}
	
	private class SightAdapter extends ArrayAdapter<Sight>
	{
		public SightAdapter(ArrayList<Sight> sights)
		{
			super(SearchSightActivity.this, 0, sights);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			
			if (convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.location_list_item, null);
			}
			
			TextView placeText = (TextView)convertView.findViewById(R.id.tv_place);
			TextView cityText = (TextView)convertView.findViewById(R.id.tv_city);
			Sight sight = getItem(position);
			
			placeText.setText(sight.getName());
			cityText.setText(sight.getCity());
			
			return convertView;
		}
	}
}
