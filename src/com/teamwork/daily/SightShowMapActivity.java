package com.teamwork.daily;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Sight;

public class SightShowMapActivity extends Activity
{
	private MapView mMapView;
	private BaiduMap mBaiduMap;
	private String mName;
	private double mLongitude;
	private double mLatitude;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sight_show_map);
		handleIntent(getIntent());
		showActionBar();
		
		mMapView = (MapView)findViewById(R.id.mapView);
		mBaiduMap = mMapView.getMap();
		mBaiduMap.setMyLocationEnabled(true);
		
		MyLocationData.Builder builder = new MyLocationData.Builder();
		builder.longitude(mLongitude)
			   .latitude(mLatitude);
		mBaiduMap.setMyLocationData(builder.build());
		BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.mark);
		MyLocationConfiguration myLocationConfiguration = new MyLocationConfiguration(LocationMode.FOLLOWING, false, bitmap);
		mBaiduMap.setMyLocationConfigeration(myLocationConfiguration);
		MapStatusUpdate update = MapStatusUpdateFactory.zoomTo(13);
		mBaiduMap.setMapStatus(update);
	}
	
	@Override  
	protected void onDestroy() 
	{  
		super.onDestroy();  
	    //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理  
	    mMapView.onDestroy();  
	}  
	
	@Override  
	protected void onResume()
	{  
	    super.onResume();  
	    //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理  
	    mMapView.onResume();  
	}  
	
	@Override  
	protected void onPause()
	{  
	    super.onPause();  
	    //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理  
	    mMapView.onPause();  
	}  
	
	private void handleIntent(Intent intent)
	{
		mName = intent.getStringExtra(Sight.NAME);
		mLongitude = intent.getDoubleExtra(Sight.LONGITUDE, 0.0);
		mLatitude = intent.getDoubleExtra(Sight.LATITUDE, 0.0);
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(mName);
	}
}
