package com.teamwork.daily;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Transport;

public class TransportDetailsActivity extends Activity 
{
	public static final String TRANSPORT_INDEX = "transport_index";
	
	public static final int DAY = 3600 * 24;
	public static final int HOUR = 3600;
	public static final int MINUTE = 60;

	private Transport mTransport;
	
	private int mSearchType;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transport_details);
		
		int transportIndex = getIntent().getIntExtra(TRANSPORT_INDEX, 0);
		mTransport = TransportManager.getInstance().getTransport(transportIndex);
		
		showActionBar();
		
		mSearchType = searchType(mTransport.getDescription());
		
		TextView startName = (TextView)findViewById(R.id.tv_transport_start);
		startName.setText(mTransport.getFromName());
		
		TextView endName = (TextView)findViewById(R.id.tv_transport_end);
		endName.setText(mTransport.getToName());
		
		TextView description = (TextView)findViewById(R.id.tv_transport_description);
		description.setText(mTransport.getDescription());
		
		TextView distance = (TextView)findViewById(R.id.tv_transport_distance);
		distance.setText(formatDistance(mTransport.getDistance()));
		
		TextView time = (TextView)findViewById(R.id.tv_transport_time);
		time.setText(formatTime(mTransport.getTime()));
		
		Button previewButton = (Button)findViewById(R.id.btn_show_map);
		previewButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(TransportDetailsActivity.this, TransportShowMapActivity.class);
				intent.putExtra(TransportShowMapActivity.SEARCH_TYPE, mSearchType);
				
				intent.putExtra(TransportShowMapActivity.START_LONGITUDE, mTransport.getFromLongitude());
				intent.putExtra(TransportShowMapActivity.START_LATITUDE, mTransport.getFromLatitude());
				
				
				intent.putExtra(TransportShowMapActivity.END_LONGITUDE, mTransport.getToLongitude());
				intent.putExtra(TransportShowMapActivity.END_LATITUDE, mTransport.getToLatitude());
			

				startActivity(intent);
			}
		});
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.transport_details);
        
	}
		
	//格式化距离
	private String formatDistance(int distance)
	{
		String s;
		if (distance > 1000)
		{
			int km = distance / 1000;
			int m = (distance % 1000) / 100;
			s = km + "." + m + "km";
		}
		else
		{
			s = distance + "m";
		}
		
		return s;
	}
	
	//格式化时间
	private String formatTime(int time)
	{
		StringBuilder sb = new StringBuilder();
		if (time / DAY != 0)
		{
			sb.append(time / DAY + "天");
			time = time % DAY;
		}
		if (time / HOUR != 0)
		{
			sb.append(time / HOUR + "小时");
			time = time % HOUR;
		}
		if (time / MINUTE != 0)
		{
			sb.append(time / MINUTE + "分");
			time = time % MINUTE;
		}
		if (time != 0)
		{
			sb.append(time + "秒");
		}
		
		return sb.toString();
	}
	
	//确定检索的类型
	private int searchType(String type)
	{
		if ("驾车".equals(type))
		{
			return 0;
		}
		if ("公交".equals(type))
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
}
