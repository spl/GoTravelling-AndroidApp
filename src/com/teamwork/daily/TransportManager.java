package com.teamwork.daily;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.teamwork.model.Transport;

public class TransportManager 
{
	private ArrayList<Transport> mTransports;
	
	private static TransportManager sTransportManager;
	
	private TransportManager()
	{
		mTransports = new ArrayList<Transport>();
	}
	
	public static TransportManager getInstance()
	{
		if (sTransportManager == null)
		{
			sTransportManager = new TransportManager();
		}
		
		return sTransportManager;
	}
	
	public void clear()
	{
		mTransports.clear();
	}
	
	public void addTransport(Transport transport)
	{
		mTransports.add(transport);
	}
	
	public Transport getTransport(int index)
	{
		return mTransports.get(index);

	}
	
	public void removeTransport(int index)
	{
		mTransports.remove(index);
	}
	
	public ArrayList<Transport> getTransports()
	{
		return  mTransports;
	}

	public void parseJsonStringToTransports(String jsonString)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArray = jsonObject.getJSONArray("transportation");
			for (int i = 0; i < jsonArray.length(); i++)
			{
				mTransports.add(new Transport(jsonArray.getJSONObject(i)));
			}
			
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
}
