package com.teamwork.favorite;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.teamwork.model.CollectionRoute;

public class CollectionRouteManager 
{

	private ArrayList<CollectionRoute> mRoutes;
	
	private static CollectionRouteManager sCollectionRouteManager;
	
	private CollectionRouteManager()
	{
		mRoutes = new ArrayList<CollectionRoute>();
	}
	
	public static CollectionRouteManager getInstance()
	{
		if (sCollectionRouteManager == null)
		{
			sCollectionRouteManager = new CollectionRouteManager();
		}
		
		return sCollectionRouteManager;
	}
	
	public ArrayList<CollectionRoute> getRoutes()
	{
		return mRoutes;
	}
	
	public void addRoute(CollectionRoute route)
	{
		mRoutes.add(route);
	}
	
	public CollectionRoute getRoute(int index)
	{
		return mRoutes.get(index);
	}
	
	public void removeRoute(int index)
	{
		mRoutes.remove(index);
	}
	
	public void parseJsonToRoutes(String jsonString)
	{
		mRoutes.clear();
		try
		{
			JSONArray jsonArray = new JSONArray(jsonString);
			JSONObject jsonObject;
			for (int i = 0; i < jsonArray.length(); i++)
			{
				jsonObject = jsonArray.getJSONObject(i);
				mRoutes.add(new CollectionRoute(jsonObject));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public void reset()
	{
		sCollectionRouteManager = null;
	}
}
