package com.teamwork.favorite;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.teamwork.gotravelling.R;

public class FavoriteActivity extends FragmentActivity
{
	private FragmentManager fm;
	private Fragment mOldFragment;
	private Button mPlaceButton;
	private Button mRouteButton;
	
	private int currentPosition = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorite);
		
		showActionBar();
		
		fm = getSupportFragmentManager();
		
		mPlaceButton = (Button)findViewById(R.id.btn_place_favorite);
		mPlaceButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (currentPosition != 0)
				{
					mPlaceButton.setBackgroundResource(android.R.color.white);
					mRouteButton.setBackgroundResource(R.color.gray);
					currentPosition = 0;
					if (mOldFragment == null)
						mOldFragment = new PlaceFavoriteFragment();
					replaceFragment(mOldFragment);
				}
			}
		});
		
		mRouteButton = (Button)findViewById(R.id.btn_route_favorite);
		mRouteButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (currentPosition != 1)
				{
					mRouteButton.setBackgroundResource(android.R.color.white);
					mPlaceButton.setBackgroundResource(R.color.gray);
					currentPosition = 1;
					if (mOldFragment == null)
						mOldFragment = new RouteFavoriteFragment();
					replaceFragment(mOldFragment);
				}
			}
		});
		
		replaceFragment(new PlaceFavoriteFragment());
	}
	
	//��ʾactionbar
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.favorites);
        
	}
	
	//����fragment��ʾ
	private void replaceFragment(Fragment fragment)
	{
		Fragment temp = fm.findFragmentById(R.id.container);
		fm.beginTransaction()
			.replace(R.id.container, fragment)
			.commit();
		mOldFragment = temp;
	}
}
