package com.teamwork.favorite;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.daily.LocationDetailsActivity;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Place;
import com.teamwork.model.Sight;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class PlaceFavoriteFragment extends Fragment
{
	private ListView mListView;
	private PlaceAdapter mPlaceAdapter;
	
	private int mCurrentPosition;
	
	private RequestQueue mQueue;
	
	private ProgressDialog mDeleteProgressDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		mDeleteProgressDialog = HandleUtil.showProgressDialog(getActivity(), "正在删除...");
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.fragment_place_favorite, null);
		mListView = (ListView)v.findViewById(R.id.lv_places);
		mPlaceAdapter = new PlaceAdapter(PlaceManager.getInstance().getPlaces());
		mListView.setAdapter(mPlaceAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
			{
				Intent intent = new Intent(getActivity(), LocationDetailsActivity.class);
				Place place = mPlaceAdapter.getItem(position);
				intent.putExtra(Sight.UID, place.getUid());
				intent.putExtra(Sight.NAME, place.getName());
				startActivity(intent);
			}
		});
		registerForContextMenu(mListView);
		if (mPlaceAdapter.getCount() == 0)
		{
			Toast.makeText(getActivity(), "当前收藏的景点列表为空", Toast.LENGTH_SHORT).show();
		}
		
		return v;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) 
	{
		getActivity().getMenuInflater().inflate(R.menu.place_list_item_context, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) 
	{
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		mCurrentPosition = info.position;
		switch (item.getItemId())
		{
		case R.id.menu_item_delete_place:
			mDeleteProgressDialog.show();
			deletePlace();
			break;
		}
		return super.onContextItemSelected(item);
	}
	
	private void deletePlace()
	{
		String cookie = UserManager.getInstance(getActivity()).getCookie();
			
		String uri = HttpUrl.PLACE_FAVORITE + "/" + mPlaceAdapter.getItem(mCurrentPosition).getId();
		HttpUtil.httpGet(mQueue, uri, Method.DELETE, cookie, new DeletePlaceResponseListener());
	}
	
	private class PlaceAdapter extends ArrayAdapter<Place>
	{
		public PlaceAdapter(ArrayList<Place> places)
		{
			super(getActivity(), 0, places);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getActivity().getLayoutInflater().inflate(R.layout.place_favorite_item, null);
			}
			
			Place place = getItem(position);
			
			TextView numberText = (TextView)convertView.findViewById(R.id.tv_number);
			numberText.setText(""+(position+1));
			
			TextView nameText = (TextView)convertView.findViewById(R.id.tv_place_name);
			nameText.setText(place.getName());
			
			return convertView;
		}
	}
	
	private class DeletePlaceResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			PlaceManager.getInstance().removePlace(mCurrentPosition);
			mPlaceAdapter.notifyDataSetChanged();
			mDeleteProgressDialog.dismiss();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mDeleteProgressDialog.dismiss();
			Toast.makeText(getActivity(), "删除路线失败", Toast.LENGTH_SHORT).show();
		}
	}
}
