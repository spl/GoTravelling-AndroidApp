package com.teamwork.favorite;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.teamwork.model.Place;


public class PlaceManager 
{
	private ArrayList<Place> mPlaces;
	
	private static PlaceManager sPlaceManager;
	
	private PlaceManager()
	{
		mPlaces = new ArrayList<Place>();
	}
	
	public static PlaceManager getInstance()
	{
		if (sPlaceManager == null)
		{
			sPlaceManager = new PlaceManager();
		}
		
		return sPlaceManager;
	}
	
	public ArrayList<Place> getPlaces()
	{
		return mPlaces;
	}
	
	public void addPlace(Place place)
	{
		mPlaces.add(place);
	}
	
	public Place getPlace(int index)
	{
		return mPlaces.get(index);
	}
	
	public void removePlace(int index)
	{
		mPlaces.remove(index);
	}
	
	public void parseJsonToPlaces(String jsonString)
	{
		mPlaces.clear();
		try
		{
			JSONArray jsonArray = new JSONArray(jsonString);
			JSONObject jsonObject;
			for (int i = 0; i < jsonArray.length(); i++)
			{
				jsonObject = jsonArray.getJSONObject(i);
				mPlaces.add(new Place(jsonObject));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public void reset()
	{
		sPlaceManager = null;
	}
}
