package com.teamwork.gotravelling;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;

import com.baidu.mapapi.SDKInitializer;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

public class MyApplication extends Application
{
	@Override
	public void onCreate() 
	{
		super.onCreate();
		// 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
		SDKInitializer.initialize(this);
		//捕获应用可能出现的异常并发送邮件反馈
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() 
	    {
			@Override
			public void uncaughtException(Thread thread, final Throwable ex) 
			{
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.printStackTrace(pw);
					
				StringBuilder sb = new StringBuilder();
					
				//设备的Android版本号
				sb.append("Version code is ");
				sb.append(Build.VERSION.SDK_INT + "\n");
				//设备型号
				sb.append("Model is ");
				sb.append(Build.MODEL + "\n");
				sb.append(sw.toString());

				Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
				sendIntent.setData(Uri.parse("mailto:1011347848@qq.com"));
				//邮件主题
				sendIntent.putExtra(Intent.EXTRA_SUBJECT, "GoTravelling Bug Report");
				//堆栈信息
				sendIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
				startActivity(sendIntent);
				//杀死进城
				android.os.Process.killProcess(android.os.Process.myPid());   
//			    System.exit(10);
			}
		});
	}
}
