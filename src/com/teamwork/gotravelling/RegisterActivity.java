package com.teamwork.gotravelling;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.model.User;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
import com.teamwork.utils.MyOnKeyListener;

public class RegisterActivity extends Activity 
{
	private EditText mUsernameText;
	private EditText mPasswordText;
	private EditText mConfirmPasswordText;
	private RequestQueue mQueue;
	
	private ProgressDialog mProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		mUsernameText = (EditText)findViewById(R.id.et_username);
		mPasswordText = (EditText)findViewById(R.id.et_password);
		mConfirmPasswordText = (EditText)findViewById(R.id.et_confirm_password);
		mConfirmPasswordText.setOnKeyListener(new MyOnKeyListener());
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		Button registerButton = (Button)findViewById(R.id.btn_register);
		registerButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				register();
			}
		});
		
		Button backButton = (Button)findViewById(R.id.btn_back);
		backButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				back();
			}
		});
		mProgressDialog = HandleUtil.showProgressDialog(this, "正在注册...");
	}
	
	//注册
	private void register()
	{
		Map<String, String> params = new HashMap<String, String>();
		String username = mUsernameText.getText().toString();
		String password = mPasswordText.getText().toString();
		String confirmPassword = mConfirmPasswordText.getText().toString();
		
		if (validate(username, password, confirmPassword))
		{
			mProgressDialog.show();
			if (HandleUtil.isPhoneNumber(username))
			{
				params.put("cellphone_number", username);
			}
			else
			{
				params.put("username", username);
			}
			params.put("password", password);
			params.put("password_confirmation", confirmPassword);
			HttpUtil.httpPostForJson(mQueue, HttpUrl.REGISTER, Method.POST, true, params, new RegisterResponseListener());
		}
	}
	
	private class RegisterResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			handleResult(response);
		}

		@Override
		public void onErrorResponse(String error) 
		{
			handleError(error);
		}
	}
	
	private void handleError(String error)
	{
		try
		{
			mProgressDialog.dismiss();
			
			JSONObject json = new JSONObject(error);
			
			if (json.has(User.NICKNAME)) //昵称已存在
			{
				Toast.makeText(this, "该昵称已存在！", Toast.LENGTH_SHORT).show();
			}
			else if (json.has(User.USERNAME)) //用户名已存在
			{
				Toast.makeText(this, "注册失败，该用户名已注册！", Toast.LENGTH_SHORT).show();
			}
			else if (json.has(User.CELL_PHONE_NUMBER)) //电话号码已存在
			{
				Toast.makeText(this, "注册失败，该电话号码已注册！", Toast.LENGTH_SHORT).show();
			}
			else if (json.has(User.PASSWORD)) //密码已存在
			{
				Toast.makeText(this, "密码错误！", Toast.LENGTH_SHORT).show();
			}
				
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	//处理结果
	private void handleResult(String result)
	{
		try
		{
			JSONObject json = new JSONObject(result);
			
			String nickname = json.getString(User.NICKNAME);
			String username = json.getString(User.USERNAME);
			String cellPhoneNumber = json.getString(User.CELL_PHONE_NUMBER);
			String sex = json.getString(User.SEX);
			String headImageBytes = json.getString(User.HEAD_IMAGE);
			String signature = json.getString(User.SIGNATURE);
			String email = json.getString(User.EMAIL);
			String cookie = json.getString(UserManager.COOKIE);
			UserManager.getInstance(this).setCookie(cookie);
			
			String[] data = headImageBytes.split("base64,");
			Bitmap bitmap = HandleUtil.base64ToBitmap(data[1]);
			
			User user = new User();
			user.setNickname(nickname);
			user.setUsername(username);
			user.setCellPhone(cellPhoneNumber);
			user.setHeadImage(bitmap);
			user.setSex(sex);
			user.setEmail(email);
			user.setSignature(signature);
			
			UserManager.getInstance(this).setUser(user);
			UserManager.getInstance(this).saveUserAccount(mUsernameText.getText().toString(), mPasswordText.getText().toString());
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		mProgressDialog.dismiss();
		finish();
	}
	
	//注册之前验证输入
	private boolean validate(String username, String password, String confirmPassword)
	{
		//用户名为空
		if (HandleUtil.isEmpty(username))
		{
			Toast.makeText(this, "请输入用户名或手机号！", Toast.LENGTH_SHORT).show();
			return false;
		}
		//密码为空
		if (HandleUtil.isEmpty(password))
		{
			Toast.makeText(this, "请输入密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		//确认密码为空
		if (HandleUtil.isEmpty(confirmPassword))
		{
			Toast.makeText(this, "请输入确认密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		//密码和确认密码不相等
		if (!password.equals(confirmPassword))
		{
			Toast.makeText(this, "请确保两次输入的密码相等！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!HandleUtil.validateLength(username))
		{
			Toast.makeText(this, "请输入6到20位的用户名！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!HandleUtil.validateLength(password))
		{
			Toast.makeText(this, "请输入6到20位的密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	//后退
	private void back()
	{
		Intent intent = new Intent(this, StartActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	public void onBackPressed() 
	{
		back();
	}

}
