package com.teamwork.gotravelling;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartActivity extends Activity
{
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		
		Button loginButton = (Button)findViewById(R.id.btn_login);
		loginButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent i = new Intent(StartActivity.this, LoginActivity.class);
				startActivity(i);
				finish();
			}
		});
		
		Button registerButton = (Button)findViewById(R.id.btn_register);
		registerButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent i = new Intent(StartActivity.this, RegisterActivity.class);
				startActivity(i);
				finish();
			}
		});
	}
}
