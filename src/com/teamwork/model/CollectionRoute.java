package com.teamwork.model;

import org.json.JSONException;
import org.json.JSONObject;

public class CollectionRoute 
{
	public static final String ID = "_id";
	public static final String OWNER_ID = "owner_id";
	public static final String ROUTE_ID = "route_id";
	public static final String CREATOR_ID = "creator_id";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String CREATED_AT = "created_at";
	
	private String mId;
	private String mOwnerId;
	private String mRouteId;
	private String mCreatorId;
	private String mName;
	private String mDescription;
	private String mCreatedAt;
	
	public CollectionRoute()
	{
		
	}
	
	public CollectionRoute(JSONObject jsonObject)
	{
		try
		{
			mId = jsonObject.getString(ID);
			mOwnerId = jsonObject.getString(OWNER_ID);
			mRouteId = jsonObject.getString(ROUTE_ID);
			mCreatorId = jsonObject.getString(CREATOR_ID);
			mName = jsonObject.getString(NAME);
			mDescription = jsonObject.getString(DESCRIPTION);
			mCreatedAt = jsonObject.getString(CREATED_AT);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public String getId() 
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getOwnerId()
	{
		return mOwnerId;
	}
	
	public void setOwnerId(String ownerId)
	{
		mOwnerId = ownerId;
	}
	
	public String getRouteId() 
	{
		return mRouteId;
	}
	
	public void setRouteId(String routeId)
	{
		mRouteId = routeId;
	}
	
	public String getCreatorId()
	{
		return mCreatorId;
	}
	
	public void setCreatorId(String creatorId)
	{
		mCreatorId = creatorId;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public void setName(String name)
	{
		mName = name;
	}
	
	public String getDescription() 
	{
		return mDescription;
	}
	
	public void setDescription(String description)
	{
		mDescription = description;
	}
	
	public String getCreatedAt()
	{
		return mCreatedAt;
	}
	
	public void setCreatedAt(String createdAt)
	{
		mCreatedAt = createdAt;
	}
	
	
}
