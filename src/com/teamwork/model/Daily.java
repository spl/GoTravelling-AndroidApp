package com.teamwork.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Daily 
{
	public static final String ID = "_id";
	public static final String REMARK = "remark";
	public static final String SIGHTS = "sights";
	public static final String SIGHT_ID = "sight_id";
	public static final String SIGHT_NAME = "name";
	public static final String SIGHT_LOC = "loc";
	public static final String SIGHT_COORDINATES = "coordinates";
	
	private String mId;
	private String mRemark;
	private ArrayList<Sight> mSights;
	
	public Daily clone()
	{
		Daily temp = new Daily();
		temp.mId = mId;
		temp.mRemark = mRemark;
		ArrayList<Sight> sights = new ArrayList<Sight>();
		for (Sight sight : mSights)
		{
			sights.add(sight);
		}
		temp.mSights = sights;
		return temp;
	}
	
	public String getId() 
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getRemark() 
	{
		return mRemark;
	}
	
	public void setRemark(String remark)
	{
		mRemark = remark;
	}
	
	public void addSight(Sight sight)
	{
		mSights.add(sight);
	}
	
	public ArrayList<Sight> getSights()
	{
		return mSights;
	}
	
	public void setSights(ArrayList<Sight> sights)
	{
		mSights = sights;
	}

	public Daily()
	{
		mSights = new ArrayList<Sight>();
	}
	
	public Daily(JSONObject jsonObject)
	{
		try
		{
			mId = jsonObject.getString(ID);
			mRemark = jsonObject.getString(REMARK);
			mSights = parseJsonToSights(jsonObject.getJSONArray(SIGHTS));
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private ArrayList<Sight> parseJsonToSights(JSONArray jsonArray) throws JSONException
	{
		ArrayList<Sight> sights = new ArrayList<Sight>();
		JSONObject jsonObject;
		Sight sight;
		for (int i = 0; i < jsonArray.length(); i++)
		{
			jsonObject = jsonArray.getJSONObject(i);
			sight = new Sight();
			if (jsonObject.has(SIGHT_ID))
			{
				sight.setId(jsonObject.getString(SIGHT_ID));
			}
			sight.setName(jsonObject.getString(SIGHT_NAME));
			JSONObject loc = jsonObject.getJSONObject(SIGHT_LOC);
			JSONArray coordinates = loc.getJSONArray(SIGHT_COORDINATES);
			sight.setLongitude(coordinates.getDouble(0));
			sight.setLatitude(coordinates.getDouble(1));
			sights.add(sight);
		}
		return sights;
	}
	
	public JSONObject toJSON()
	{
		try
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(ID, mId);
			jsonObject.put(REMARK, mRemark);
			jsonObject.put(SIGHTS, parseSightsToJson());
			return jsonObject;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private JSONArray parseSightsToJson() throws JSONException
	{
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject;
		for (Sight sight : mSights)
		{
			jsonObject = new JSONObject();
			jsonObject.put(SIGHT_ID, sight.getId());
			jsonObject.put(SIGHT_NAME, sight.getName());
			JSONObject loc = new JSONObject();
			loc.put("type", "Point");
			JSONArray locArray = new JSONArray();
			locArray.put(0, sight.getLongitude());
			locArray.put(1, sight.getLatitude());
			loc.put(SIGHT_COORDINATES, locArray);
			jsonObject.put(SIGHT_LOC, loc);
			jsonArray.put(jsonObject);
		}
		return jsonArray;
	}
	
}
