package com.teamwork.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.teamwork.utils.HandleUtil;

public class Place 
{
	public static final String ID = "_id";
	public static final String NAME = "name";
	public static final String LONGITUDE = "longitude";
	public static final String LATITUDE = "latitude";
	public static final String ADDRESS = "address";
	public static final String UID = "uid";
	public static final String CITY = "city";
	
	private String mId;
	private String mName;
	private double mLongitude;
	private double mLatitude;
	private String mAddress;
	private String mUid;
	private String mCity;
	
	public Place()
	{
		
	}
	
	public Place(JSONObject jsonObject)
	{
		try
		{
			mId = jsonObject.getString(ID);
			mName = jsonObject.getString(NAME);
			mLongitude = jsonObject.getDouble(LONGITUDE);
			mLatitude = jsonObject.getDouble(LATITUDE);
			mUid = jsonObject.getString(UID);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public JSONObject toJSON()
	{
		try
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(NAME, mName);
			jsonObject.put(LONGITUDE, mLongitude);
			jsonObject.put(LATITUDE, mLatitude);
			jsonObject.put(UID, mUid);
			if (mAddress == null)
				mAddress = "";
			jsonObject.put(ADDRESS, mAddress);
			if (!HandleUtil.isEmpty(mCity))
			{
				jsonObject.put(CITY, mCity);
			}
			return jsonObject;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public String getId() 
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public void setName(String name)
	{
		mName = name;
	}
	
	public double getLongitude() 
	{
		return mLongitude;
	}
	
	public void setLongitude(double longitude)
	{
		mLongitude = longitude;
	}
	
	public double getLatitude() 
	{
		return mLatitude;
	}
	
	public void setLatitude(double latitude)
	{
		mLatitude = latitude;
	}
	
	public String getAddress()
	{
		return mAddress;
	}
	
	public void setAddress(String address)
	{
		mAddress = address;
	}
	
	public String getUid() 
	{
		return mUid;
	}
	
	public void setUid(String uid)
	{
		mUid = uid;
	}
	
	public String getCity() 
	{
		return mCity;
	}
	
	public void setCity(String city)
	{
		mCity = city;
	}
}
