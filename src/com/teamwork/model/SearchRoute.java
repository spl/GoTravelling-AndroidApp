package com.teamwork.model;

import org.json.JSONException;
import org.json.JSONObject;

public class SearchRoute 
{
	public static final String ID = "_id";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String CREATOR = "creator";
	public static final String CREATOR_ID = "_id";
	public static final String USERNAME = "username";
	public static final String CELLPHONE_NUMBER = "cellphone_number";
	public static final String CREATED_AT = "created_at";
	public static final String HEAD_IMAGE = "head_image";
	
	private String mId;
	private String mName;
	private String mDescription;
	private String mCreatorId;
	private String mUsername;
	private String mCellphoneNumber;
	private String mHeadImage;
	private String mCreatedAt;
	
	public SearchRoute()
	{
		
	}
	
	public SearchRoute(JSONObject jsonObject)
	{
		try
		{
			mId = jsonObject.getString(SearchRoute.ID);
			mName = jsonObject.getString(SearchRoute.NAME);
			mDescription = jsonObject.getString(SearchRoute.DESCRIPTION);
			
			JSONObject creator = jsonObject.getJSONObject(SearchRoute.CREATOR);
			mCreatorId = creator.getString(SearchRoute.CREATOR_ID);
			if (creator.has(SearchRoute.USERNAME))
			{
				mUsername = creator.getString(SearchRoute.USERNAME);
			}
			if (creator.has(SearchRoute.CELLPHONE_NUMBER))
			{
				mCellphoneNumber = creator.getString(SearchRoute.CELLPHONE_NUMBER);
			}
			mCreatedAt = jsonObject.getString(SearchRoute.CREATED_AT);
			mHeadImage = creator.getString(SearchRoute.HEAD_IMAGE);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public String getId()
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public void setName(String name)
	{
		mName = name;
	}
	
	public String getDescription() 
	{
		return mDescription;
	}
	
	public void setDescription(String description) 
	{
		mDescription = description;
	}
	
	public String getCreatorId()
	{
		return mCreatorId;
	}
	
	public void setCreatorId(String creatorId)
	{
		mCreatorId = creatorId;
	}
	
	public String getUsername()
	{
		return mUsername;
	}
	
	public void setUsername(String username)
	{
		mUsername = username;
	}
	
	public String getCellphoneNumber() 
	{
		return mCellphoneNumber;
	}
	
	public void setCellphoneNumber(String cellphoneNumber)
	{
		mCellphoneNumber = cellphoneNumber;
	}
	
	public String getHeadImage()
	{
		return mHeadImage;
	}
	
	public void setHeadImage(String headImage)
	{
		mHeadImage = headImage;
	}
	
	public String getCreatedAt()
	{
		return mCreatedAt;
	}
	
	public void setCreatedAt(String createdAt) 
	{
		mCreatedAt = createdAt;
	}
	
	
}
