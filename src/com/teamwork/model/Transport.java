package com.teamwork.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Transport 
{
	public static final String ID = "_id";
	public static final String FROM_NAME = "from_name";
	public static final String FROM_SIGHT_ID = "from_sight_id";
	public static final String FROM_LOC = "from_loc";
	public static final String COORDINATES = "coordinates";
	public static final String TO_NAME = "to_name";
	public static final String TO_SIGHT_ID = "to_sight_id";
	public static final String TO_LOC = "to_loc";
	public static final String DESCRIPTION = "description";
	public static final String DISTANCE = "prize";
	public static final String TIME = "consuming";
	
	private String mId;
	private String mFromName;
	private String mFromSightId;
	private double mFromLongitude;
	private double mFromLatitude;
	private String mToName;
	private String mToSightId;
	private double mToLongitude;
	private double mToLatitude;
	private String mDescription;
	private int mDistance;
	private int mTime;
	
	public Transport()
	{
		
	}
	
	public Transport(JSONObject jsonObject)
	{
		try
		{
			mId = jsonObject.getString(ID);
			mFromName = jsonObject.getString(FROM_NAME);
			mFromSightId = jsonObject.getString(FROM_SIGHT_ID);
			JSONObject fromLoc = jsonObject.getJSONObject(FROM_LOC);
			JSONArray fromLocArray = fromLoc.getJSONArray(COORDINATES);
			mFromLongitude = fromLocArray.getDouble(0);
			mFromLatitude = fromLocArray.getDouble(1);
			mToName = jsonObject.getString(TO_NAME);
			mToSightId = jsonObject.getString(TO_SIGHT_ID);
			JSONObject toLoc = jsonObject.getJSONObject(TO_LOC);
			JSONArray toLocArray = toLoc.getJSONArray(COORDINATES);
			mToLongitude = toLocArray.getDouble(0);
			mToLatitude = toLocArray.getDouble(1);
			mDescription = jsonObject.getString(DESCRIPTION);
			mDistance = jsonObject.getInt(DISTANCE);
			mTime = jsonObject.getInt(TIME);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	public JSONObject toJSON()
	{
		try
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(ID, mId);
			jsonObject.put(FROM_NAME, mFromName);
//			jsonObject.put(FROM_SIGHT_ID, mFromSightId);
			JSONObject fromLoc = new JSONObject();
			fromLoc.put("type", "Point");
			JSONArray fromLocArray = new JSONArray();
			fromLocArray.put(0, mFromLongitude);
			fromLocArray.put(1, mFromLatitude);
			fromLoc.put(COORDINATES, fromLocArray);
			jsonObject.put(FROM_LOC, fromLoc);
			
			jsonObject.put(TO_NAME, mToName);
//			jsonObject.put(TO_SIGHT_ID, mToSightId);
			JSONObject toLoc = new JSONObject();
			toLoc.put("type", "Point");
			JSONArray toLocArray = new JSONArray();
			toLocArray.put(0, mToLongitude);
			toLocArray.put(1, mToLatitude);
			toLoc.put(COORDINATES, toLocArray);
			jsonObject.put(TO_LOC, toLoc);
			
			jsonObject.put(DESCRIPTION, mDescription);
			jsonObject.put(DISTANCE, mDistance);
			jsonObject.put(TIME, mTime);
			return jsonObject;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public String getId() 
	{
		return mId;
	}
	
	public void setId(String id) 
	{
		mId = id;
	}
	
	public String getFromName() 
	{
		return mFromName;
	}
	
	public void setFromName(String fromName) 
	{
		mFromName = fromName;
	}
	
	public String getFromSightId()
	{
		return mFromSightId;
	}
	
	public void setFromSightId(String fromSightId)
	{
		mFromSightId = fromSightId;
	}
	
	public double getFromLongitude() 
	{
		return mFromLongitude;
	}
	
	public void setFromLongitude(double fromLongitude)
	{
		mFromLongitude = fromLongitude;
	}
	
	public double getFromLatitude() 
	{
		return mFromLatitude;
	}
	
	public void setFromLatitude(double fromLatitude)
	{
		mFromLatitude = fromLatitude;
	}
	
	public String getToName() 
	{
		return mToName;
	}
	
	public void setToName(String toName) 
	{
		mToName = toName;
	}
	
	public String getToSightId() 
	{
		return mToSightId;
	}
	
	public void setToSightId(String toSightId) 
	{
		mToSightId = toSightId;
	}
	
	public double getToLongitude() 
	{
		return mToLongitude;
	}
	
	public void setToLongitude(double toLongitude)
	{
		mToLongitude = toLongitude;
	}
	
	public double getToLatitude() 
	{
		return mToLatitude;
	}
	
	public void setToLatitude(double toLatitude) 
	{
		mToLatitude = toLatitude;
	}
	
	public String getDescription() 
	{
		return mDescription;
	}
	
	public void setDescription(String description) 
	{
		mDescription = description;
	}
	
	public int getDistance()
	{
		return mDistance;
	}
	
	public void setDistance(int distance) 
	{
		mDistance = distance;
	}
	
	public int getTime()
	{
		return mTime;
	}
	
	public void setTime(int time) 
	{
		mTime = time;
	}
	
	
	
}
