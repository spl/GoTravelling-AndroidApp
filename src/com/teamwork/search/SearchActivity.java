package com.teamwork.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.teamwork.gotravelling.R;

public class SearchActivity extends FragmentActivity 
{
	
	private Button mSightButton;
	private Button mRouteButton;
	
	private int currentPosition = 0;
	
	private FragmentManager fm;
	private Fragment mOldFragment;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		
		showActionBar();
		
		fm = getSupportFragmentManager();
		
		mSightButton = (Button)findViewById(R.id.btn_sight);
		mSightButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (currentPosition != 0)
				{
					mSightButton.setBackgroundResource(android.R.color.white);
					mRouteButton.setBackgroundResource(R.color.gray);
					currentPosition = 0;
					if (mOldFragment == null)
						mOldFragment = new SightSearchFragment();
					replaceFragment(mOldFragment);
				}
			}
		});
		
		mRouteButton = (Button)findViewById(R.id.btn_route);
		mRouteButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (currentPosition != 1)
				{
					mRouteButton.setBackgroundResource(android.R.color.white);
					mSightButton.setBackgroundResource(R.color.gray);
					currentPosition = 1;
					if (mOldFragment == null)
						mOldFragment = new RouteSearchFragment();
					replaceFragment(mOldFragment);
				}
			}
		});
		
		replaceFragment(new SightSearchFragment());
	}
	
	//����fragment��ʾ
	private void replaceFragment(Fragment fragment)
	{
		Fragment temp = fm.findFragmentById(R.id.container);
		fm.beginTransaction()
			.replace(R.id.container, fragment)
			.commit();
		mOldFragment = temp;
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.quick_search);
        
	}
}
