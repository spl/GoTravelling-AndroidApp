package com.teamwork.search;

import java.util.ArrayList;

import com.teamwork.model.SearchRoute;

public class SearchRouteManager
{
	private ArrayList<SearchRoute> mRoutes;
	
	private static SearchRouteManager sSearchRouteManager;
	
	private SearchRouteManager()
	{
		mRoutes = new ArrayList<SearchRoute>();
	}
	
	public static SearchRouteManager getInstance()
	{
		if (sSearchRouteManager == null)
		{
			sSearchRouteManager = new SearchRouteManager();
		}
		
		return sSearchRouteManager;
	}
	
	public void clear()
	{
		mRoutes.clear();
	}
	
	public void addRoute(SearchRoute route)
	{
		mRoutes.add(route);
	}
	
	public SearchRoute getRoute(int index)
	{
		return mRoutes.get(index);
	}
	
	public ArrayList<SearchRoute> getRoutes()
	{
		return mRoutes;
	}
}
