package com.teamwork.sign;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.teamwork.gotravelling.R;
import com.teamwork.utils.HandleUtil;

public class SelectPictureActivity extends Activity
{
	private ArrayList<String> mImages = new ArrayList<String>();
	private ArrayList<Bitmap> mBitmaps = new ArrayList<Bitmap>();
	private ArrayList<String> mSelected = new ArrayList<String>();
	
	private GridView mGridView;
	private ImageAdapter mImageAdapter;
	private BitmapFactory.Options mOptions;
	private TextView mCompletedText;
	private ProgressDialog mProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_picture);
		
		mOptions = new BitmapFactory.Options();
		mOptions.inSampleSize = 4;
		
		mProgressDialog = HandleUtil.showProgressDialog(this, "正在获取图片...");
		showActionBar();
		mSelected.clear();
		
		mGridView = (GridView)findViewById(R.id.gridView);
		
		mCompletedText = (TextView)findViewById(R.id.tv_right);
		
		mProgressDialog.show();
		
		new MyTask().execute();
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.select_image);
        
	}
	
	private void getImages()
	{
		Cursor cursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI, null, null, null, null);
		String path;
		while (cursor.moveToNext())
		{
			byte[] data = cursor.getBlob(cursor.getColumnIndex(Media.DATA));
			path = new String(data, 0, data.length-1);
			mImages.add(path);
		}
	}
	
	private void updateCompletedText()
	{
		String text = "完成(" + mSelected.size() + "/" + mImages.size() + ")";
		mCompletedText.setText(text);
	}
	
	private class ImageAdapter extends ArrayAdapter<String>
	{
		public ImageAdapter(ArrayList<String> images)
		{
			super(SelectPictureActivity.this, 0, images);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			final ViewHolder viewHolder;
			if (convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.select_picture_list_item, null);
				viewHolder = new ViewHolder();
				
				viewHolder.imageView = (ImageView)convertView.findViewById(R.id.iv_image);
				viewHolder.imageButton = (ImageButton)convertView.findViewById(R.id.ib_select);
				
				convertView.setTag(viewHolder);
			}
			else
			{
				viewHolder = (ViewHolder)convertView.getTag();
			}
			
			final String path = getItem(position);
			
			viewHolder.imageView.setImageBitmap(mBitmaps.get(position));
			
			viewHolder.imageView.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					if (mSelected.contains(path))
					{
						viewHolder.imageButton.setImageResource(R.drawable.picture_unselected);
						mSelected.remove(path);
					}
					else
					{
						viewHolder.imageButton.setImageResource(R.drawable.pictures_selected);
						mSelected.add(path);
					}
					updateCompletedText();
				}
			});
			
			return convertView;
		}
	}
	
	private class ViewHolder
	{
		public ImageView imageView;
		public ImageButton imageButton;
	}
	
	private class MyTask extends AsyncTask<Void, Void, Void>
	{

		@Override
		protected Void doInBackground(Void... arg0)
		{
			getImages();
			for (String path : mImages)
			{
				mBitmaps.add(BitmapFactory.decodeFile(path, mOptions));
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			mProgressDialog.dismiss();
			mImageAdapter = new ImageAdapter(mImages);
			mGridView.setAdapter(mImageAdapter);
			updateCompletedText();
		}
		
	}
}
