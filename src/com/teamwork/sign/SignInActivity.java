package com.teamwork.sign;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.Route;
import com.teamwork.user.UserManager;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class SignInActivity extends Activity
{
	private static final int REQUEST_CAMERA_IMAGE = 1;
	
	private TextView mRouteNameText;
	private TextView mProgressText;
	private ListView mSightList;
	private SightAdapter mSightAdapter;
	
	private RequestQueue mQueue;
	
	private ProgressDialog mGetProgressDialog;
	
	private Route mCurrentRoute;
	
	private File mSaveImageFile; //调用相机时头像保存的路径
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);
		
		mRouteNameText = (TextView)findViewById(R.id.tv_route_name);
		mProgressText = (TextView)findViewById(R.id.tv_progress);
		mSightList = (ListView)findViewById(R.id.lv_sights);
		mSightAdapter = new SightAdapter(new ArrayList<CheckSight>());
		mSightList.setAdapter(mSightAdapter);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		mGetProgressDialog = HandleUtil.showProgressDialog(this, "正在获取...");
		
		showActionBar();
		
		getPersonalRoutes();
	}
	
	private void showActionBar()
	{
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.sign_in);
	}

	private void getPersonalRoutes()
	{
		mGetProgressDialog.show();
		String cookie = UserManager.getInstance(this).getCookie();
		
		HttpUtil.httpJsonGetForArray(mQueue, HttpUrl.ROUTE, cookie, "type=mine", new RouteResponseListener());
	}
	
	//上传图片
	private void uploadImage()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View view = getLayoutInflater().inflate(R.layout.select_image, null);
		((TextView)view.findViewById(R.id.tv_title)).setText(R.string.upload_image);
		builder.setView(view);
		final AlertDialog dialog = builder.create();
		dialog.show();
		Button cancelButton = (Button)dialog.findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		
		RelativeLayout photoImageLayout = (RelativeLayout)dialog.findViewById(R.id.rl_phone_image);
		photoImageLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				Intent intent = new Intent(SignInActivity.this, SelectPictureActivity.class);
				startActivity(intent);
			}
		});
		
		RelativeLayout takePhotoLayout = (RelativeLayout)dialog.findViewById(R.id.rl_take_photo);
		takePhotoLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra("autofocus", true); //自动对焦  
	            intent.putExtra("fullScreen", false); //全屏  
	            intent.putExtra("showActionIcons", false);  
	            String filename = UUID.randomUUID().toString() + ".jpg";
	            mSaveImageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), filename);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mSaveImageFile));
				startActivityForResult(intent, REQUEST_CAMERA_IMAGE);
			}
		});
	}
	
	private class SightAdapter extends ArrayAdapter<CheckSight>
	{
		public SightAdapter(ArrayList<CheckSight> sights)
		{
			super(SignInActivity.this, 0, sights);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = getLayoutInflater().inflate(R.layout.sign_in_list_item, null);
			}
			CheckSight sight = getItem(position);
			TextView sightName = (TextView)convertView.findViewById(R.id.tv_place_name);
			sightName.setText(sight.name);
			CheckBox checkBox = (CheckBox)findViewById(R.id.cb_sign_in);
			if (sight.isCheck)
			{
				checkBox.setChecked(true);
			}
			
			Button uploadButton = (Button)convertView.findViewById(R.id.btn_upload_picture);
			uploadButton.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v) 
				{
					uploadImage();
				}
			});
			return convertView;
		}
	}
	
	private void parseJsonToRoutes(String jsonString)
	{
		try
		{
			JSONArray jsonArray = new JSONArray(jsonString);
			
			if (jsonArray.length() <= 0)
			{
				mGetProgressDialog.dismiss();
				Toast.makeText(SignInActivity.this, "你还没开始任何路线", Toast.LENGTH_SHORT).show();
				return;
			}
			
			for (int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				
				mCurrentRoute = new Route();
				if ((jsonObject.getString(Route.STATUS).equals("travelling")))
				{
					mCurrentRoute = new Route(jsonObject);
					
					String cookie = UserManager.getInstance(this).getCookie();
				
					String uri = HttpUrl.ROUTE + "/" + mCurrentRoute.getId();
					HttpUtil.httpGet(mQueue, uri, Method.GET, cookie, new GetDailyResponseListener());
					return;
				}				
			}
			mGetProgressDialog.dismiss();
			Toast.makeText(SignInActivity.this, "你还没开始任何路线", Toast.LENGTH_SHORT).show();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private void parseJsonToSights(String jsonString)
	{
		try
		{
			JSONObject tempObject = new JSONObject(jsonString);
			JSONArray jsonArray = tempObject.getJSONArray("daily");
			
			mRouteNameText.setText(mCurrentRoute.getName());
			
			if (jsonArray.length() <= 0)
			{
				Toast.makeText(SignInActivity.this, "当前已添加景点列表为空", Toast.LENGTH_SHORT).show();
				return;
			}
			
			ArrayList<CheckSight> sights;
			for (int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				sights = parseJsonToSights(jsonObject.getJSONArray("sights"));
				mSightAdapter.addAll(sights);
			}
			
			mSightAdapter.notifyDataSetChanged();
			
			mProgressText.setText("进度：" + getHaveSigned() + "/" + mSightAdapter.getCount());
				
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	private int getHaveSigned()
	{
		int count = 0;
		for (int i = 0; i < mSightAdapter.getCount(); i++)
		{
			if (mSightAdapter.getItem(i).isCheck)
			{
				count++;
			}
		}
		return count;
	}
	
	private ArrayList<CheckSight> parseJsonToSights(JSONArray jsonArray)
	{
		ArrayList<CheckSight> sights = new ArrayList<CheckSight>();
		try
		{
			JSONObject jsonObject;
			CheckSight sight;
			for (int i = 0; i < jsonArray.length(); i++)
			{
				jsonObject = jsonArray.getJSONObject(i);
				sight = new CheckSight(jsonObject);
				sights.add(sight);
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		return sights;
	}
	
	public static class CheckSight
	{
		public String id;
		public String name;
		public double longitude;
		public double latitude;
		public boolean isCheck;
		
		public CheckSight()
		{
			
		}
		
		public CheckSight(JSONObject jsonObject)
		{
			try
			{
				if (jsonObject.has("sight_id"))
				{
					id = jsonObject.getString("sight_id");
				}
				name = jsonObject.getString("name");
				if (jsonObject.has("check_in"))
				{
					isCheck = jsonObject.getBoolean("check_in");
				}
				JSONObject loc = jsonObject.getJSONObject("loc");
				JSONArray jsonArray = loc.getJSONArray("coordinates");
				longitude = jsonArray.getDouble(0);
				latitude = jsonArray.getDouble(1);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	private class GetDailyResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mGetProgressDialog.dismiss();
			parseJsonToSights(response);
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mGetProgressDialog.dismiss();
			Toast.makeText(SignInActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
	
	private class RouteResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			parseJsonToRoutes(response);
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mGetProgressDialog.dismiss();
			Toast.makeText(SignInActivity.this, "网络连接超时", Toast.LENGTH_SHORT).show();
		}
	}
}
