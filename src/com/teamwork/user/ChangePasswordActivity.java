package com.teamwork.user;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.User;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;
import com.teamwork.utils.MyOnKeyListener;

public class ChangePasswordActivity extends Activity
{
	private EditText mCurrentPasswordText;
	private EditText mNewPasswordText;
	private EditText mConfirmPasswordText;
	private Button mOkButton;
	
	private RequestQueue mQueue;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		
		findViewById();
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.change_password);
        
        mOkButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View arg0) 
			{
				if (validate())
				{
					hideSoftInput();
					changePassword();
				}
			}
		});
	}
	
	private void findViewById()
	{
		mCurrentPasswordText = (EditText)findViewById(R.id.et_current_password);
		mNewPasswordText = (EditText)findViewById(R.id.et_new_password);
		mConfirmPasswordText = (EditText)findViewById(R.id.et_confirm_password);
		mConfirmPasswordText.setOnKeyListener(new MyOnKeyListener());
		
		mOkButton = (Button)findViewById(R.id.btn_ok);
	}
	
	//验证
	private boolean validate()
	{
		//获取用户的密码
		String password = PreferenceManager.getDefaultSharedPreferences(this).getString(User.PASSWORD, null);
		
		String currentPassword = mCurrentPasswordText.getText().toString();
		if (HandleUtil.isEmpty(currentPassword)) //判断当前密码是否为空
		{
			Toast.makeText(this, "请输入当前密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!currentPassword.equals(password)) //判断当前密码是否正确
		{
			Toast.makeText(this, "当前密码不正确！", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		String newPassword = mNewPasswordText.getText().toString();
		String confirmPassword = mConfirmPasswordText.getText().toString();
		
		if (HandleUtil.isEmpty(newPassword)|| HandleUtil.isEmpty(confirmPassword)) //判断输入的密码是否为空
		{
			Toast.makeText(this, "新的密码不能为空！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!newPassword.equals(confirmPassword)) //判断两次的密码输入是否相等
		{
			Toast.makeText(this, "请确保两次输入的密码相等！", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (currentPassword.equals(newPassword)) //判断新密码是否与旧密码相同
		{
			Toast.makeText(this, "新的密码与当前密码相等！ ", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!HandleUtil.validateLength(newPassword))
		{
			Toast.makeText(this, "请输入6到20位的密码！", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	//更改密码
	private void changePassword()
	{
		Map<String, String> params = new HashMap<String, String>();
		
		String currentPassword = mCurrentPasswordText.getText().toString();
		String newPassword = mNewPasswordText.getText().toString();
		String confirmPassword = mConfirmPasswordText.getText().toString();
		
		params.put("password", currentPassword);
		params.put("newPassword", newPassword);
		params.put("newPassword_confirmation", confirmPassword);
		params.put(UserManager.COOKIE, UserManager.getInstance(this).getCookie());
		
		HttpUtil.httpPostForJson(mQueue, HttpUrl.PASSWORD, Method.PUT, false, params, new ChangePasswordResponseListener());
	}
	
	private void hideSoftInput()
	{
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);  
		  
		if(imm.isActive())
		{  
			imm.hideSoftInputFromWindow(mConfirmPasswordText.getApplicationWindowToken(), 0 );
		}  
	}
	
	private class ChangePasswordResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			Toast.makeText(ChangePasswordActivity.this, "密码修改成功！", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			//error==null,表明修改密码成功,因为密码修改返回为空不能转化为json,导致JSONException
			if (error == null)
			{
				Toast.makeText(ChangePasswordActivity.this, "密码修改成功！", Toast.LENGTH_SHORT).show();
				//修改保存的密码
				UserManager.getInstance(ChangePasswordActivity.this).changePassword(mNewPasswordText.getText().toString());
				finish();
			}
			else
			{
				Toast.makeText(ChangePasswordActivity.this, "密码修改失败！", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
