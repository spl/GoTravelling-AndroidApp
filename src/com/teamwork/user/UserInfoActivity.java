package com.teamwork.user;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.teamwork.gotravelling.R;
import com.teamwork.model.User;
import com.teamwork.utils.HandleUtil;
import com.teamwork.utils.HttpResponseListener;
import com.teamwork.utils.HttpUrl;
import com.teamwork.utils.HttpUtil;

public class UserInfoActivity extends Activity 
{
	public static final String HEAD_BITMAP_BYTE = "bitmap_byte"; 
	
	private static final int REQUEST_PHOTO_IMAGE = 1;
	private static final int REQUEST_CLIP_PICTURE = 2;
	private static final int REQUEST_CAMERA_IMAGE = 3;
	
	private LayoutInflater mInflater;
	private TextView mUsernameText;
	private TextView mNicknameText;
	private TextView mSignatureText;
	private TextView mSexText;
	private TextView mEmailText;
	private ImageView mHeadView;
	
	private User mUser; //保存用户信息
	
	private File mSaveHeadImageFile; //调用相机时头像保存的路径
	
	private boolean mIsChangeUserInfo = false; //判断用户信息是否更改
	
	private ProgressDialog mHeadImageChangeDialog;
	
	private RequestQueue mQueue;
	
	private Bitmap mBitmap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_info);
		
		mQueue = Volley.newRequestQueue(this.getApplicationContext());
		
		mHeadImageChangeDialog = HandleUtil.showProgressDialog(this, "正在上传头像...");
		
		mInflater = getLayoutInflater();
		findViewById();
		
		Button backButton = (Button)findViewById(R.id.btn_actionbar);
        backButton.setBackgroundResource(R.drawable.btn_back);
        backButton.setOnClickListener(new View.OnClickListener()
        {
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
        
        TextView textView = (TextView)findViewById(R.id.tv_actionbar);
        textView.setText(R.string.user_info);
        
        RelativeLayout nicknameLayout = (RelativeLayout)findViewById(R.id.rl_nickname);
        nicknameLayout.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				String oldNickname = mNicknameText.getText().toString();
				AlertDialog.Builder builder = new AlertDialog.Builder(UserInfoActivity.this);
				builder.setTitle("修改昵称");
				LinearLayout ll = (LinearLayout)mInflater.inflate(R.layout.edit_text, null);
				final EditText nicknameEdit = (EditText)ll.findViewById(R.id.edit_text);
				nicknameEdit.setHint(oldNickname);
				builder.setView(ll);
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						String nickname = nicknameEdit.getText().toString();
						if (HandleUtil.isEmpty(nickname))
						{
							Toast.makeText(UserInfoActivity.this, "昵称不能为空！", Toast.LENGTH_SHORT).show();
						}
						else
						{
							mNicknameText.setText(nickname);
							mUser.setNickname(nickname);
							mIsChangeUserInfo = true;
						}
					}
				});
				builder.setNegativeButton(R.string.cancel, null);
				builder.create().show();
			}
		});
        
        RelativeLayout signatureLayout = (RelativeLayout)findViewById(R.id.rl_signature);
        signatureLayout.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				String oldSignature = mSignatureText.getText().toString();
				AlertDialog.Builder builder = new AlertDialog.Builder(UserInfoActivity.this);
				builder.setTitle("修改个性签名");
				LinearLayout ll = (LinearLayout)mInflater.inflate(R.layout.edit_text, null);
				final EditText signatureEdit = (EditText)ll.findViewById(R.id.edit_text);
				signatureEdit.setHint(oldSignature);
				
				builder.setView(ll);
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						String signature = signatureEdit.getText().toString();
						if (HandleUtil.isEmpty(signature))
						{
							Toast.makeText(UserInfoActivity.this, "个性签名不能为空！", Toast.LENGTH_SHORT).show();
						}
						else
						{
							mSignatureText.setText(signature);
							mUser.setSignature(signature);
							mIsChangeUserInfo = true;
						}
					}
				});
				builder.setNegativeButton(R.string.cancel, null);
				builder.create().show();
			}
		});
        
        RelativeLayout emailLayout = (RelativeLayout)findViewById(R.id.rl_email);
        emailLayout.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				String oldEmail = mEmailText.getText().toString();
				AlertDialog.Builder builder = new AlertDialog.Builder(UserInfoActivity.this);
				builder.setTitle("修改邮箱");
				LinearLayout ll = (LinearLayout)mInflater.inflate(R.layout.edit_text, null);
				final EditText emailEdit = (EditText)ll.findViewById(R.id.edit_text);
				emailEdit.setHint(oldEmail);
				builder.setView(ll);
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						String email = emailEdit.getText().toString();
						if (HandleUtil.isEmpty(email))
						{
							Toast.makeText(UserInfoActivity.this, "邮箱不能为空！", Toast.LENGTH_SHORT).show();
						}
						else
						{
							setEmailText(email);
							mUser.setEmail(email);
							mIsChangeUserInfo = true;
						}
					}
				});
				builder.setNegativeButton(R.string.cancel, null);
				builder.create().show();
			}
		});
        
        RelativeLayout sexLayout = (RelativeLayout)findViewById(R.id.rl_sex);
        sexLayout.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				String oldSex = mSexText.getText().toString();
				int index = 0;
				if ("女".equals(oldSex))
				{
					index = 1;
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(UserInfoActivity.this);
				builder.setTitle("性别");
				builder.setSingleChoiceItems(new String[]{"男", "女"}, index, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						setSexText(which);
						mIsChangeUserInfo = true;
					}
				});
				builder.setPositiveButton(R.string.ok, null);
				final int oldSexIndex = index;
				builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						setSexText(oldSexIndex);
					}
				});
				builder.create().show();
			}
		});
        
        RelativeLayout passwordLayout = (RelativeLayout)findViewById(R.id.rl_password);
        passwordLayout.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(UserInfoActivity.this, ChangePasswordActivity.class);
				startActivity(intent);
			}
		});
	
        mHeadView.setOnClickListener(new View.OnClickListener()
        {	
			@Override
			public void onClick(View v)
			{
				changeHeadView();
			}
		});
	
        setUserInfo();
	}
	
	@Override
	public void onStop()
	{
		super.onStop();
		//更新用户信息
		UserManager.getInstance(this).setUser(mUser);
		if (mIsChangeUserInfo)
		{
			putUserInfo();
		}
		
	}
	
	//初始化用户信息
	private void setUserInfo()
	{
		mUser = UserManager.getInstance(this).getUser();
		
		String username = mUser.getUsername();
		if (HandleUtil.isEmpty(username))
		{
			username = mUser.getCellPhone();
		}
		mUsernameText.setText(username);
		
		mHeadView.setImageBitmap(mUser.getHeadImage());
		
		String nickName = mUser.getNickname();
		if (HandleUtil.isEmpty(nickName))
		{
			nickName = "请设置你的昵称";
		}
		mNicknameText.setText(nickName);
		
		String signature = mUser.getSignature();
		if (HandleUtil.isEmpty(signature))
		{
			signature = "请设置你的个性签名";
		}
		mSignatureText.setText(signature);
		
		String sex = mUser.getSex();
		if (HandleUtil.isEmpty(sex))
		{
			sex = "男";
			mUser.setSex("male");
			mIsChangeUserInfo = true;
		}
		else if ("male".equals(sex))
		{
			sex = "男";
		}
		else 
		{
			sex = "女";
		}
		
		mSexText.setText(sex);
		
		String email = mUser.getEmail();
		if (HandleUtil.isEmpty(email))
		{
			email = "请设置你的邮箱";
		}
		
		mEmailText.setText(email);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_PHOTO_IMAGE && resultCode == RESULT_OK) //使用手机相册返回的图片
		{
			Uri uri = data.getData();
			if (uri != null)
			{
				Intent intent = new Intent(UserInfoActivity.this, ClipPictureActivity.class);
				intent.setData(uri);
				startActivityForResult(intent, REQUEST_CLIP_PICTURE);
			}
		}
		else if (requestCode == REQUEST_CLIP_PICTURE && resultCode == RESULT_OK) //裁剪图片返回的数据
		{
			byte[] bitmapByte = data.getByteArrayExtra(HEAD_BITMAP_BYTE);
			mBitmap = BitmapFactory.decodeByteArray(bitmapByte, 0, bitmapByte.length);
			
			putHeadImage();
			
		}
		else if (requestCode == REQUEST_CAMERA_IMAGE && resultCode == RESULT_OK) //使用相机拍摄返回的图片
		{
			Intent intent = new Intent(UserInfoActivity.this, ClipPictureActivity.class);
			scan();
			intent.setData(Uri.fromFile(mSaveHeadImageFile));
			startActivityForResult(intent, REQUEST_CLIP_PICTURE);
		}
	}

	private void findViewById()
	{
		mUsernameText = (TextView)findViewById(R.id.tv_username);
		mNicknameText = (TextView)findViewById(R.id.tv_nickname);
		mSignatureText = (TextView)findViewById(R.id.tv_signature);
		mSexText = (TextView)findViewById(R.id.tv_sex);
		mEmailText = (TextView)findViewById(R.id.tv_email);
		mHeadView = (ImageView)findViewById(R.id.iv_head);
	}
	
	//设置email
	private void setEmailText(String s)
	{
		if (HandleUtil.isEmail(s))
		{
			mEmailText.setText(s);
		}
		else
		{
			Toast.makeText(this, "请输入正确的邮箱!", Toast.LENGTH_SHORT).show();
		}
	}

	//设置性别
	private void setSexText(int which)
	{
		if (which == 0)
		{
			mSexText.setText("男");
			mUser.setSex("male");
		}
		else
		{
			mSexText.setText("女");
			mUser.setSex("female");
		}
	}
	
	//更改头像，弹出对话框
	private void changeHeadView()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View view = mInflater.inflate(R.layout.select_image, null);
		builder.setView(view);
		final AlertDialog dialog = builder.create();
		dialog.show();
		Button cancelButton = (Button)dialog.findViewById(R.id.btn_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		
		RelativeLayout photoImageLayout = (RelativeLayout)dialog.findViewById(R.id.rl_phone_image);
		photoImageLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, REQUEST_PHOTO_IMAGE);
			}
		});
		
		RelativeLayout takePhotoLayout = (RelativeLayout)dialog.findViewById(R.id.rl_take_photo);
		takePhotoLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra("autofocus", true);//自动对焦  
	            intent.putExtra("fullScreen", false);//全屏  
	            intent.putExtra("showActionIcons", false);  
	            String filename = UUID.randomUUID().toString() + ".jpg";
	            mSaveHeadImageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), filename);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mSaveHeadImageFile));
				startActivityForResult(intent, REQUEST_CAMERA_IMAGE);
			}
		});
	}
	
	//更改用户信息时同步到web后台
	private void putUserInfo()
	{
		Map<String, String> params = new HashMap<String, String>();
		
		params.put(User.NICKNAME, mUser.getNickname());
		params.put(User.SEX, mUser.getSex());
		params.put(User.EMAIL, mUser.getEmail());
		params.put(User.SIGNATURE, mUser.getSignature());
		params.put(UserManager.COOKIE, UserManager.getInstance(this).getCookie());
		
		HttpUtil.httpPostForJson(mQueue, HttpUrl.USER_INFO, Method.PUT, false, params, new UserInfoResponseListener());
	}
	
	//更改用户头像时同步到web后台
	private void putHeadImage()
	{
		mHeadImageChangeDialog.show();
		try
		{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("head_image", HandleUtil.bitmapToBase64(mBitmap));
			String cookie = UserManager.getInstance(this).getCookie();
			HttpUtil.httpJson(mQueue, HttpUrl.HEAD_IMAGE, Method.POST, cookie, jsonObject, new HeadImageResponseListener());
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
	
	//将图片图片添加到媒体库
	private void scan()
	{
		MediaScannerConnection.scanFile(this, new String[] { mSaveHeadImageFile.toString() }, null,
                new MediaScannerConnection.OnScanCompletedListener()
		{
            public void onScanCompleted(String path, Uri uri) 
            {
                
            }
        });

	}
	
	private class UserInfoResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
		}

		@Override
		public void onErrorResponse(String error) 
		{
		}
	}
	
	private class HeadImageResponseListener implements HttpResponseListener
	{

		@Override
		public void onResponse(String response)
		{
			mHeadImageChangeDialog.dismiss();
			mUser.setHeadImage(mBitmap);
			mHeadView.setImageBitmap(mBitmap);
			Toast.makeText(UserInfoActivity.this, "头像上传成功", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onErrorResponse(String error) 
		{
			mHeadImageChangeDialog.dismiss();
			if (error == null)
			{
				mUser.setHeadImage(mBitmap);
				mHeadView.setImageBitmap(mBitmap);
				Toast.makeText(UserInfoActivity.this, "头像上传成功", Toast.LENGTH_SHORT).show();
			}
			else
			{
				Toast.makeText(UserInfoActivity.this, "头像上传失败", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
}
