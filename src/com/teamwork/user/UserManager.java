package com.teamwork.user;

import java.util.HashMap;
import java.util.Map;

import com.teamwork.model.User;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserManager
{
	public static final String COOKIE = "cookie";
	
	private Context mContext;
	private User mUser;
	private String mCookie;
	
	private static UserManager sUserManager;
	
	private UserManager(Context context)
	{
		mContext = context;
	}
	
	public static UserManager getInstance(Context context)
	{
		if (sUserManager == null)
		{
			sUserManager = new UserManager(context);
		}
		
		return sUserManager;
	}
	
	public void setUser(User user)
	{
		mUser = user;
	}
	
	public User getUser()
	{
		return mUser;
	}
	
	//保存用户账号登信息，以便下次自动登录 
	public void saveUserAccount(String username, String password)
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(User.USERNAME, username);
		editor.putString(User.PASSWORD, password);
		editor.putBoolean(User.IS_SAVE, true);
		editor.apply();
	}
	
	//更改密码
	public void changePassword(String password)
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(User.PASSWORD, password);
		editor.apply();
	}
	
	//获得用户账号信息
	public Map<String, String> getUserAccount()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		Map<String, String> params = new HashMap<String, String>();
		String username = sharedPreferences.getString(User.USERNAME, null);
		String password = sharedPreferences.getString(User.PASSWORD, null);
		params.put("identify", username);
		params.put("password", password);
		return params;
	}

	//是否保存了账号
	public boolean isSaveUserAccount()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		return sharedPreferences.getBoolean(User.IS_SAVE, false);
	}
	
	//退出该账号
	public void exitUserAccount()
	{
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(User.IS_SAVE, false);
		editor.apply();
	}

	public String getCookie() 
	{
		return mCookie;
	}

	public void setCookie(String cookie)
	{
		mCookie = cookie;
	}
	
	
}
