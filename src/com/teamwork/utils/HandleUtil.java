package com.teamwork.utils;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class HandleUtil 
{
	/**
	 * 判断字符串是否为空
	 * @param s 普通字符串
	 * @return 返回true或者false
	 */
	public static boolean isEmpty(String s)
	{
		if (s != null && !s.equals(""))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
	 * 判断是否为电话号码
	 * @param s 普通字符串
	 * @return 返回true或者false
	 */
	public static boolean isPhoneNumber(String s)
	{
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(s); 
		return m.matches();
	}
	
	/**
	 * 判断是否为邮箱
	 * @param s 普通字符串
	 * @return 返回true或者false
	 */
	public static boolean isEmail(String s)
	{
		Pattern p = Pattern.compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|" +
				"(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
		Matcher m = p.matcher(s);
		return m.matches();
	}
	
	/**
	 * 用base64解密headImageBytes数据额并转为bitmap 
	 * @param headImageBytes
	 * @return
	 */
	public static Bitmap base64ToBitmap(String headImageBytes)
	{
		byte[] bytes = Base64.decode(headImageBytes, Base64.DEFAULT);  
	    return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
	}
	
	/**
	 * 将bitmap用base64加密
	 * @param bitmap
	 * @return
	 */
	public static String bitmapToBase64(Bitmap bitmap)
	{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		//将Bitmap压缩成PNG编码，质量为100%存储
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
		String headImageBytes = Base64.encodeToString(os.toByteArray(), Base64.DEFAULT);
		return headImageBytes;
	}
	
	/**
	 * 判断字符串的长度是否在6-20之间
	 * @param s 普通字符串
	 * @return
	 */
	public static boolean validateLength(String s)
	{
		int length = s.length();
		if (length > 20 || length < 6)
			return false;
		else
			return true;
	}
	
	/**
	 * 初始化进度对话框
	 * @param context
	 * @return ProgressDialog对象
	 */
	public static ProgressDialog showProgressDialog(Context context, String message)
	{
		ProgressDialog progressDialog = new ProgressDialog(context); 
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setIndeterminate(true);
		return progressDialog;
	}
	
	/**
	 * 隐藏输入键盘
	 * @param v 通常为EditText
	 */
	public static void hideSoftInput(View v)
	{
		InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);  
		  
		if(imm.isActive())
		{  
			imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0 );
		}   
	}
}
